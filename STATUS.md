
#Аналоги

* todoist и wunderlist

	* можно создавать подзадачи
	* можно открывать доступ другим к какой-либо группе задач
	* можно назначать задачам исполнителей 
	* писать комментарии.

* todoist и tick-tick
	* удобный просмотр и редактирование дел на сегодня и на следующие 7 дней

* tick-tick и wunderlist
	* можно добавить срок окончания задачи

* tick-tick
	* есть более объемное описание задачи
	* есть календарь(в премиум версии)
	* можно добавить время для дела и напоминание за 5 минут, за полчаса, за опреленное кол-во дней итд. и настроить пользовательское напоминание

* todoist
	* удобное редактирование дерева задач 
минусы:
	* нет календаря
	* напоминания только в премиум версии
	* нельзя добавить время задаче

* гугл календарь 
	* есть возможность создавать мероприятия, приглашать других пользователей(и получать ответ на предложение)
	* есть возможность автоматически подбирать время для периодических дел и составлять расписание с учетом пожеланий пользователя.


#Сценарии использования

* создать задачу "писать лабораторную"
	* установить на дату понедельник
	* время начала на 17:00, окончания - на 20:00
	* напоминание в 17.00

* создать задачу "сделать лабораторную"
	* установить дату начала в понедельник и датой окончания в воскресенье
	* напоминанием в понедельник и в четверг в 13:30

* создать задачу "позвонить кому-либо завтра" 
	* добавить время выполнения 16:00
	* добавить напоминание в 15:50 завтра

* создать задачу "сходить на танцы" 
	* установить дату на субботу
	* указать время с 12.00 до 15.00 
	* установить интервал повторения, равный неделе начиная с этой даты и времени

* создать задачу "выбрать подарки друзьям на Новый год"
	* установить дату начала 10 декабря, дату окончания 31 декабря
	* напоминаниями 10, 15 и 20 декабря
	* установить интервал повторения каждый год 
	
* создать задачу "подготовка к выступлению на студдебюте"
	* с датой начала 1 сентября и окончания 25 октября
	* Добавить подзадачи "найти костюмы", "нарисовать плакаты", "найти место для репитициий".
	* Разрешить Васе, Насте и Паше добавлять другие подзадачи и назначить каждой подзадаче своих исполнителей.


* создать задачи "встретиться со школьными друзьями","сходить на маникюр", "приехать домой на выходные".
	* установить последней задаче дату 30 марта
	* При создании или редактировании первых двух настроить напоминания, когда выполнится задача "приехать домой на выходные"

* создать задачи "купить рубашку" и "купить ботинки".
	* создать задачу "поехать за покупками в воскресенье" 
	* сделать первые две задачи ее подзачами.

#Возможности программы

##Минимум:

###Прототип
* Создание задачи. 
	* Добавление краткого описания(обязательно) и более обширного(по желанию)
	* Добавление задачи в одну или несколько смысловых групп
	* Выставление приоритета(ниизкий, средний, высокий)
	* По желанию присвоение даты и времени начала и окончания задачи(можно создать задачу без временных ограничений)
	* Задача может быть подзадачей другого дела

* Создание периодических задач с указанием интервала(день, неделя, месяц, год)

* Создание и удаление групп задач

* Просмотр задач в определенной группе

* Редактирование существующих дел
	* Изменять положение задачи в дереве
	* Сдвигать сроки выполнения, редактировать описание
	* Изменять группы, к которым задача принадлежит 
	* Удалить задачу

###Базовая версия
* Авторизация пользователей

* Изменять статус дела(не начато, начато, в процессе, выполнено, провалено). При создании задачи по умолчанию устанавливается статус "не начато".

* Создание новых пользователей

* Открывать другим пользователям доступ в определенную группу и назначать исполнителей задач из этой группы



###Веб-версия

* Аунтефикация пользователей

* Напоминания для задач

* Уведомления об изменении статуса задач других пользователей, к которым у пользователя есть доступ


##Максимум

###Базовая версия

* создание связи "одна задача блокирует другую" в групповых задачах либо если пользователю предоставлено право просмотра дела другого пользователя.

* сортировка по приоритету и дате окончания

* фильтрация по статусу выполнения.

* Настройка времени и количества уведомлений

* Открывать пользователям доступ на просмотр определенного дела

* Просматривать дела на определенный день или на следующие 7 дней

* Поиск задач по первым буквам их названия

###Веб-версия
* Создание новостной ленты, где будут отражаться все уведомления

* Возможность оставлять комментарии под задачами других пользователей

* Назначение исполнителей задач посредством отправки сообщений на электронную почту

* Добавление различных режимов просмотра календаря(по месяцам, неделям и дням)

* Просмотр статистики: сколько дел нужно выполнить, начать или закончить на этой неделе(в этом месяце). 



#UI

##Веб-версия
###Создание/редактирование задачи

Задача в режиме создания и редактирования выглядит одинаково и в веб-версии будет представлять собой следующее.

Поле для ввода названия задачи.
Снизу поля: 
	* "описание"
	* "календарь"
	* "группы"
	* "статус"
	* "приоритет"
	* "напоминания"


1) Календарь 
	* кнопки "date" и "due" обозначающие два режима выбора дат и времени начала и окончания дела соответственно.
		* для каждого режима открывается свой календарь
		* и поле для ввода времени чере двоеточие.
		


2)Группы
	* список всех групп
	* около каждой группы checkbox, означающий добавление либо удаление из группы

3)Приоритет
	* список: низкий, средний, высокий. По умолчанию устанавливается средний приоритет

4)Напоминания
	* список уже установленных напоминаний 
		* кнопкой "удалить" справа от каждого из них
	* внизу всех напоминаний - кнопка "добавить".
	По нажатию кнопки "добавить" либо по нажатию на какое-либо напоминание из списка, появляется 			* маленький календарик 
		* поле для ввода времени.
5)Статус
	* список для выбора статуса c возможными значениями


###Отображение задачи

Задача отображается следующим образом.
	* строка с названием задачи. 
	* справа deadline
	* справа приоритет отображается кружочком определенного цвета(красный - высокий приоритет, желтый, зеленый).
	* Слева статус
		* отображается маленькой кнопкой с первой буквой статуса( P- In process, F-Failed)
		* также для каждого статуса цвет
		* по нажатию статус поднимается на следующий уровень( с Not started на In process, 			например)

	* Справа от названия кнопка, по нажатию список:
		* переход в режим детального просмотра задачи и редактирования
		* удалить
		* архивировать

	* Проваленные задачи имеют серый фон.

###Отображение задач в режиме календаря

Когда пользователь находится в режиме календаря, 
	* по умолчанию отображаются задачи на сегодня. 
	* вверху есть кнопки 
		* "сегодня"
		* "завтра"
		* "следующие 7 дней"
		* "календарь"
			* по нажатии календарик, где можно выбрать дату
		* "добавить".

Задачи на определенный день отображаются следующим образом
	* сначала задачи, у которых дата начала совпадает с выбранной датой
	* затем те, у которых дата окончания совпадает с выбранной датой
	* в каждом из списков в порядке возрастания по времени. 

###Отображение и редактирование задач в режиме группы

При просмотре задач в какой-либо группе они показываются в столбец, где отношения "задача-подзадача" отображаются отступами. Например:

* Задача 1
	* подзадача 2
		* подзадача 3
			* подзадача 4
			* подзадача 5
		* подзадача 6
			* подзадача 7
				* подзадача 8
	* подзадача 9


По аналогии с todoist редактирование иерархии задач происходит следующим образом.

При помощи описанных выше опций в режиме отображения, можно добавить новую задачу выше либо ниже по положению относительно определенной задачи.

Отступы при редактировании новой задачи определяются опциями parent_task либо sub_task.

Когда пользователь выбирает сделать задачу sub_task, она "подвигается вправо".


Например, вызвав sub_task у подзадачи из 6 строчки нашего примера, получим

* Задача 1
	* подзадача 2
		* подзадача 3
	    	* подзадача 4
			* подзадача 5
			* подзадача 6
			* подзадача 7
				* подзадача 8
	* подзадача 9


При нажатии parent_task у задачи, она и все ее подзадачи "подвигаются влево".



Нажав parent_task у задачи из строчки 3 нашего примера, получим

* Задача 1
	* подзадача 2
	* подзадача 3
		* подзадача 4
		* подзадача 5
		* подзадача 6
		* подзадача 7
			* подзадача 8
	* подзадача 9



По таким же правилам можно перемещать задачу в дереве при помощи перетягивания влево,вправо, вверх и вниз. Задача перетягивается вместе со своими подзадачами, которые на время перетягивания скрываются. Потенциальное положение задачи при ее перемещении будет показываться серым прямоугольником в определенном месте иерархии.


###Групповые дела

Создание групповых дел происходит посредством открытия доступа к какой-либо группе определенным пользователям. 
В режиме просмотра задач группы:
	* вверху есть кнопка "share"
		* по нажатии поле поиска пользователей
		* при выборе пользователя
			* его имя добавляется на специальную панель внизу
			* поле поиска очищается
		* кнопка "ок"
			* все выбранные пользователи добавляются в группу

У всех задач в совместной группе, в списке опций при нажатии кнопки "...", появляется вариант "назначить исполнителя". 
* При нажатии снова открывается список участников группы
* сверху отображаются уже заданные исполнители задачи.
*  В каждой строке, соответствующей пользователю, стоят checkbox
посредством которого можно добавлять или удалять исполнителей.
* Выше списка поле для поиска пользователя.
* По умолчанию исполнителем является создатель задачи.
* У задачи обязательно должен быть хотя бы 1 исполнитель.

##Прототип и базовая версия

Пользовательский интерфейс в прототипе и базовой версии программы будет представляться консолью.
1) Запуск программы
* При запуске программы пользователю будет предложено авторизоваться - ввести логин и пароль(в базовой версии). 
* Далее будет предлагаться различные варианты(создать задачу, войти в режим календаря, либо в режим просмотра групп).

2)Просмотр групп
* В режиме просмотра групп, отображается их название и id
	* пользователь вводит id группы для просмотра задач в ней.

3)Просмотр календаря
* В режиме календаря, необходимо выбрать одну из опций 
	* "сегодня"
	* "завтра"
	* "следующие 7 дней"
	* "ввести дату".

4)Отображение задач
После выбора, в обоих режимах будут отображаться задачи и их атрибуты в строчку.
Иерархия, как и в веб-версии, отображается отступами.

 Выше задач - опции 
* "редактировать задачу"
* "добавить задачу"
* "редактировать дерево задач"(в режиме группы) итд, 
* а также опции для перехода в другой режим. 

5)Создание, редактирование задач
При создании/редактировании задачи опциями будут ее атрибуты и опция "ок". 

При редактировании дерева -
* "добавить ниже"(потом ввести id задачи, ниже которой добавляем)
* "добавить выше"
* "sub-task"
* "parent-task"
* "поместить между"(аналог перетягивания вверх - вниз задач в веб-версии).

* При выполнении каждой из опций, снова выводятся все доступные опции.

#Логическая архитектура

##Сущности 

* задача
	* id
	* название
	* описание
	* сроки и время начала и окончания выполнения
	* приоритет
	* статус
	* parent-задача
	* список подзадач
	* список групп, в которых она состоит
	* исполнитель
	* план(может не быть)


* группа
	* id
	* название
	* задачи, которые ей принадлежат
	* список пользователей

* план
	* id
	* название
	* период(день, неделя, месяц, год)
	* последняя созданная задача
	* созданные задачи
	* владелец плана
 
* пользователь 
	* id
	* логин 
	* пароль
	* адрес электронной почты
	* ник-нейм
	* список групп
	* список задач
	* список планов
	* default-группа

В базовой версии программы будет существовать возможность сохранения информации в базу данных.Для каждого типа будет создана отдельная таблица, содержащая атрибуты сущностей, а также таблицы для сохранения связей между ними.

Связь задача-задача
1) id задачи
2) id подзадачи

Связь группа-задача
1) id группы
2) id задачи

Связь задача-пользователь
1) id задачи
2) id пользователя

Связь группа-пользователь
1) id группы
2) id пользователя

Связь план-задача
1) id задачи
2) id плана

Связь план-пользователь
1) id плана
2) id пользователя

##Группы

* Задачи могут находится в нескольких группах.
* Есть default-группа, которая содержит все задачи
	* группу удалить нельзя
	* автоматически создается при создании пользователя
	* при удалении задачи из группы, она удаляется полностью

###Добавление задач в группу

* Задача добавляется в список задач в группе
* Группа добавляется в список групп задачи.
* Нельзя добавить задачу в группу, если в ней уже есть ее предок-задача
* Если в группе есть подзадача добавляемой задачи, то подзадача удаляется и сама задача занимает ее место
* только исполнитель задачи может добавлять ее в другие группы

###Добавление пользователей в группу
* Пользователь получает права на редактирование всех задач в группе
	* кроме изменения статуса задачи
	* и добавления в другие группы

###Удаление пользователей из группы
* из группы удаляются все задачи пользователя
* если он единственный пользоваель в группе, он не может удалиться из нее, но может удалить саму группу

##Задачи

###Смена исполнителя задачи
* Изменить исполнителя можно только на того пользователя, который находится в группе, где есть эта задача
* Для всех задач поддерева, где задача является корнем:
	* удалить из групп, где нет нового пользователя
* Задача добавляется в default-группу нового пользователя

#Редактирование дерева

Рассмотрим подробнее описанные выше способы редактирования иерархии задач.

* Добавить задачу a ниже задачи b
	Если у задачи b нет подзадач, то a становится правым братом задачи b.(Т.е ее родителем становится родитель задачи b и она вставляется в список его подзадач на место после b. Если у b нет родителя, то это означает добавление в список задач текущей группы на место после задачи b). 
Если у b есть подзадачи, то a становится подзадачей b.

* Добавить задачу a выше задачи b
	Задача a становится левым братом задачи b

* Sub-task
	Сначала проверяется, есть ли у задачи parent-task и принадлежит ли она текущей группе. Если первое верно, а второе нет, то это означает, что пользователь хочет сделать задачу подзадачей еще одного дела. В таком случае ничего меняться не будет.	
Если у задачи нет parent-task, либо она находится в текущей группе(т.е редактирование проходит в рамках существующего поддерева), то задача становится подзадачей своего левого "брата", если он существует.( либо подзадачей предшествующего дела в списке дел текущей группы).


* Parent-task
	Если задача принадлежит текущей группе(т.е уровень вложенности равен нулю в данной группе), то ничего не происходит. Если же нет, то задача становится правым братом своего родителя и родителем всех своих правых братьев.

* Вставить существующую задачу "x" между двумя другими задачами "a" и "b"
Задачи a и b могут быть братьями(a является левым братом b), либо a является родителем b.

Задача x вставляется между двумя задачами всегда только на уровне вложенности задачи b. Т.е она становится левым братом задачи b. Таким образом, поменять родителя задачи можно, поднимая либо опуская ее по дереву вместе с ее поддеревом. 




#Статус
1. Рассмотрены существующие аналоги
2. Продуманы возможности программы
3. Продумана логика редактирования иерархии задач, их распределения по смысловым группам.
4. Описаны основные сущности.
5. Написаны основные классы и начата работа над интерфейсом в прототипе
