from os.path import join, dirname
from setuptools import setup, find_packages


setup(
    name='TaskTracker',
    version='1.3.55',
    packages=find_packages(),
    description='To-do tracker',
    test_suite='tests',
    url='https://bitbucket.org/DziyanaKhodar/task_tracker',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points='''
        [console_scripts]
        track=tasktracker.console.main:main
    ''',
	python_requires ='~=3.5',
	install_requires=['django==1.9','psutil==5.4.5','mysqlclient==1.3.12']
)

