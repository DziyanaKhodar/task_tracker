import logging
import sys

from tasktracker import configuration as config


def catch_log_exception(message):
	"""
	Catch any exception, that occured and log the given message and initial message of that exception
	:param message: Message that will be logged
	:return: decorator for a function
	"""
	
	def decorator(func):
		def new_func(*args):
			try:
				func(*args)
			except Exception as ex:
				logger = get_logger()
				logger.error(message)
				logger.error(ex)
		
		return new_func
	return decorator


def get_logger():
	logger_name = config.LOG_SETTINGS['LOGGER_NAME']
	return logging.getLogger(logger_name)


def bootstrap_logger():
	
	"""Bootstrap the logger so that it writes to the file, specified in configuration
	and to console with ERROR logging level"""
	
	datetime_pattern = config.DATETIME_SETTINGS['DATETIME_PATTERN']
	
	log_level = config.LOG_SETTINGS['LOG_LEVEL']
	file_log_pattern = config.LOG_SETTINGS['FILE_LOG_PATTERN']
	log_path = config.LOG_SETTINGS['LOG_PATH ']
	console_log_pattern = config.LOG_SETTINGS['CONSOLE_LOG_PATTERN']
	
	logger = get_logger()
	logger.setLevel(level=log_level)
	
	file_formatter = logging.Formatter(file_log_pattern, datetime_pattern)
	file_handler = logging.FileHandler(log_path)
	file_handler.setFormatter(file_formatter)
	file_handler.setLevel(level=log_level)
	logger.addHandler(file_handler)
	
	console_formatter = logging.Formatter(console_log_pattern)
	stream_handler = logging.StreamHandler(sys.stderr)
	stream_handler.setLevel(logging.ERROR)
	stream_handler.setFormatter(console_formatter)
	logger.addHandler(stream_handler)
	
	logger.propagate = False

