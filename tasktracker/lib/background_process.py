import errno
import os
import signal
import psutil

from daemon import DaemonContext
from daemon.pidfile import PIDLockFile

from tasktracker.lib.custom_exceptions import NotAllowedActionException


class BackgroundProcess:
	
	"""The class is used to daemonize a function by using start method or kill an existing daemon."""
	
	def __init__(self, pid_path):
		"""
		:param pid_path: path to a file, where pid of a daemon process is/will be stored
		"""
		
		self.pid_path = pid_path

	def __read_pid(self):
		try:
			with open(self.pid_path, 'r') as f:
				pid = int(f.read())
		except Exception as e:
			os.remove(self.pid_path)
			raise ValueError('Invalid value in pid file')
		
		return pid

	def start(self, func, *args):
		"""
		Start a new process that will execute the given function
		Write pid of new process to self.pid_path
		"""
		
		if self.is_running():
			raise NotAllowedActionException('Process already running')
			
		with  DaemonContext(pidfile=PIDLockFile(path=self.pid_path)):
			func(*args)
	
	def stop(self):
		"""
		Read the pid from file
		Kill the process, if it exists
		"""
		if not self.is_running():
			raise NotAllowedActionException('Process is not running')
		
		pid = self.__read_pid()
		try:
			os.kill(pid, signal.SIGTERM)
		except OSError as err:
			if err.errno == errno.ESRCH:
				raise NotAllowedActionException('Process is not running')
		
	def is_running(self):
		if not os.path.isfile(self.pid_path):
			return False
		
		pid = self.__read_pid()
		exists = psutil.pid_exists(pid)
		if not exists:
			os.remove(self.pid_path)
		return exists
		
		
		
		
		
			