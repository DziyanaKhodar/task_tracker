"""
Module represents API for working with database.
There is an ability to retrieve entities by their IDs and filter by different
parameters.


For example, in order to get all tasks in group with id=5, which should be started earlier than 2018-06-01,
you can use:

result = get_group_tasks_earlier_than(5, datetime.datetime(2018,6,1))

If you want to see all archived tasks of some user with id 'some_user_id', that had high priority, you can use:

result = get_user_archived_tasks_with_priority('some_user_id', Task.Priority.HIGH)



Also there is an ability to create a valid database for task tracker application and define,
which one should be used during the session.


"""


import django.db
import MySQLdb
from django.db.transaction import Atomic, on_commit
from django.db import connections
from django.core.management import call_command

import tasktracker.lib.models as models
import tasktracker.lib.custom_exceptions as custom_exceptions


def __establish_connection(db_info):
	name = db_info['NAME']
	connections._databases[name] = db_info
	

def get_db_info(db_name, dbuser_name, password=None, host=None, port=None):
	if host is None:
		host = 'localhost'
		
	if port is None:
		port = ''
		
	if password is None:
		password = ''
		
	return {'ENGINE': 'django.db.backends.mysql',
			'NAME': db_name,
			'USER': dbuser_name,
			'PASSWORD': password,
			'HOST': host,
			'PORT': port}


class __DatabaseRouter:
	DB_NAME = None
	
	def db_for_read(self, model, **hints):
		return self.DB_NAME
	
	def db_for_write(self, model, **hints):
		return self.DB_NAME
	
	def allow_relation(self, obj1, obj2, **hints):
		return None
	
	def allow_migrate(self, db, app_label, model_name=None, **hints):
		return True


def establish_db_connection(db_info=None):
	
	"""
	Define the database to be used for all queries by specifying it's description
	:param name: the name to identify database
	:param db: dictionary, that describes database characteristics, for example:
	{
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'mydb',
		'USER': 'root',
		'PASSWORD': 'mypassword',
		'HOST': 'localhost',
		'PORT': '3306'
	}
	
	"""
	name = 'default'
	
	if db_info is not None:
		name = db_info['NAME']
		__establish_connection(db_info)
	
	__DatabaseRouter.DB_NAME = name


def create_db(name, dbuser_name, password=None, host=None, port=None):
	"""
	Create a database with the given name, using the given dbuser_name and password of
	some mysql user.
	
	Create all needed tables for entities, present in the application
	
	If host is None, than 'localhost' will be used.
	If port is None, than 3306 will be used
	If password is None, that it is assumed, that mysql user has access without any password.
	
	Usage example:
	
	create_db('my_db','diana','mypassword')
	
	or
	
	create_db('my_db', 'diana', 'mypassword', 'localhost', 3306)
	
	"""
	
	if password is None:
		password = ''
	
	if host is None:
		host = 'localhost'
		
	if port is None:
		port = 3306
	
	db_connection = MySQLdb.connect(host=host, user=dbuser_name, password=password, port=port)
	cursor = db_connection.cursor()
	query = 'CREATE DATABASE {}'.format(name)
	cursor.execute(query)
	db_info = get_db_info(name, dbuser_name, password)
	__establish_connection(db_info)
	__migrate(name)


def __migrate(name):
	call_command('migrate', '--database={}'.format(name))
	

def __not_found_custom_exception(func):
	
	"""Raise custom exception according to entity type if the entity is not found in db by id"""
	
	def new_func(type, id, *args, **kwargs):
		try:
			return func(type, id, *args, **kwargs)
		except type.DoesNotExist:
			if type is models.Task:
				raise custom_exceptions.TaskNotFoundException('Task {} is not found'.format(id))
			elif type is models.Group:
				raise custom_exceptions.GroupNotFoundException('Group {} is not found'.format(id))
			elif type is models.Plan:
				raise custom_exceptions.PlanNotFoundException('Plan {} is not found'.format(id))
			elif type is models.User:
				raise custom_exceptions.UserNotFoundException('User {} is not found'.format(id))
	
	return new_func
	

@__not_found_custom_exception
def get_by_id(type, id):
	
	""" Get entity of defined typed by id"""
	
	return type.objects.get(id=id)


def get_task_by_id(id):
	return get_by_id(models.Task, id)


def get_group_by_id(id):
	return get_by_id(models.Group, id)


def get_plan_by_id(id):
	return get_by_id(models.Plan, id)


def get_user_by_id(id):
	return get_by_id(models.User, id)


def start_transaction(savepoint=True):
	
	"""
	Start a database transaction. if an exception raised during the transction, every change will be canceled
	If savepoint is false roll outer transaction even if the exception is caught
	"""
	
	return Atomic(__DatabaseRouter.DB_NAME, savepoint)


@__not_found_custom_exception
def select_for_update(type, id):
	
	"""Get an entity of the given type by id and block it until the transaction is commited or rolled back """
	
	return type.objects.select_for_update().get(id=id)


def select_task_for_update(task_id):
	return select_for_update(models.Task, task_id)


def select_group_for_update(group_id):
	return select_for_update(models.Group, group_id)


def select_user_for_update(user_id):
	return select_for_update(models.User, user_id)


def select_plan_for_update(plan_id):
	return select_for_update(models.Plan, plan_id)


@__not_found_custom_exception
def select_for_update_with_related(type, id, related_name):
	return type.objects.select_for_update().select_related(related_name).get(id=id)

	
def get_all(type):
	
	""" Get all entities of the given type present in the database"""
	
	return type.objects.all()


def get_all_tasks():
	return get_all(models.Task)


def get_all_groups():
	return get_all(models.Group)


def get_all_users():
	return get_all(models.User)


def get_all_plans():
	return get_all(models.Plan)


def delete_entities(entities):
	entities.delete()


def save_entity(entity):
	entity.save()


def check_user_exists(name):
	return models.User.objects.filter(id=name).exists()


def on_commit_perform(func):
	on_commit(func)


def update(entity, **kwargs):
	"""Set the attributes of the given entity to those values, that are present in **kwargs"""
	for key in kwargs.keys():
		setattr(entity, key, kwargs[key])
	
	save_entity(entity)


def close_old_connections():
	django.db.close_old_connections()
	
	
def get_all_not_archived_unfinished_tasks():
	return models.Task.objects.filter(archived=False).exclude(db_status=models.Task.Status.FAILED.name)


def get_not_archived_user_tasks(user_id):
	return models.Task.objects.filter(user=user_id).exclude(archived=True).filter(planner=None)


def get_all_archived_user_tasks(user_id):
	return models.Task.objects.filter(user=user_id).exclude(archived=False)
	
	
def get_user_tasks_by_first_letters(user_id, letters):
	return models.Task.objects.filter(user__id=user_id, name__startswith=letters).exclude(archived=True)


def get_user_archived_tasks_by_first_letters(user_id, letters):
	return models.Task.objects.filter(user__id=user_id, name__startswith=letters).exclude(archived=False)


def get_group_tasks_by_first_letters(group_id, letters):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(name__startswith=letters)

	
def get_user_groups_by_first_letters(user_id, letters):
	user = models.User.objects.get(id=user_id)
	groups = user.groups
	return groups.filter(name__startswith=letters)


def get_user_tasks_for_date(user_id, date):
	return models.Task.objects.filter(user__id=user_id, start_datetime__contains=date).exclude(archived=True)


def get_group_tasks_for_date(group_id, date):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(start_datetime__contains=date)


def get_user_archived_tasks_for_date(user_id, date):
	return models.Task.objects.filter(user__id=user_id, start_datetime__contains=date).exclude(archived=False)


def get_user_tasks_earlier_than(user_id, datetime):
	return models.Task.objects.filter(user__id=user_id, start_datetime__lte=datetime).exclude(archived=True)


def get_user_tasks_later_than(user_id, datetime):
	return models.Task.objects.filter(user__id=user_id, start_datetime__gte=datetime).exclude(archived=True)


def get_user_archived_tasks_earlier_than(user_id, datetime):
	return models.Task.objects.filter(user__id=user_id, start_datetime__lte=datetime).exclude(archived=False)


def get_user_archived_tasks_later_than(user_id, datetime):
	return models.Task.objects.filter(user__id=user_id, start_datetime__gte=datetime).exclude(archived=False)


def get_group_tasks_earlier_than(group_id, datetime):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(start_datetime__lte=datetime)


def get_group_tasks_later_than(group_id, datetime):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(start_datetime__gte=datetime)


def get_group_executor_tasks(group_id, user_id):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(user__id=user_id)


def get_group_tasks_with_status(group_id, status):
	group = get_group_by_id(group_id)
	tasks = group.tasks
	str_status = status.name
	return tasks.filter(db_status=str_status)


def get_user_tasks_with_status(user_id, status):
	str_status = status.name
	return models.Task.objects.filter(user__id=user_id, db_status=str_status)


def get_user_tasks_with_priority(user_id, priority):
	str_priority = priority.name
	return models.Task.objects.filter(user__id=user_id, db_priority=str_priority).exclude(archived=True)


def get_user_archived_tasks_with_priority(user_id, priority):
	str_priority = priority.name
	return models.Task.objects.filter(user__id=user_id, db_priority=str_priority).exclude(archived=False)


def get_group_tasks_with_priority(group_id, priority):
	str_priority = priority.name
	group = get_group_by_id(group_id)
	tasks = group.tasks
	return tasks.filter(db_priority=str_priority)


def get_user_groups(user_id):
	user = models.User.objects.get(id=user_id)
	return user.groups.all()


def check_task_in_user_groups(task_id, user_id):
	user = get_user_by_id(user_id)
	task = get_task_by_id(task_id)

	common_groups = set(task.groups.all()) & set(user.groups.all())
	return len(common_groups) != 0


def get_user_tasks(user_id):
	user = models.User.objects.get(id=user_id)
	return user.tasks.all()


def get_group_tasks(group_id):
	group = models.Group.objects.get(id=group_id)
	return group.tasks.all()

def get_user_default_group(user_id):
	user = models.User.objects.get(id=user_id)
	return user.default_group

def get_user_plans(user_id):
	user = models.User.objects.get(id=user_id)
	return user.plans.all()