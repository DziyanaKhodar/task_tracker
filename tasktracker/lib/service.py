import collections

import tasktracker.lib.managers.group_manager as group_manager
import tasktracker.lib.managers.task_manager as task_manager
import tasktracker.lib.managers.plan_manager as plan_manager
import tasktracker.lib.managers.user_manager as user_manager

import tasktracker.lib.storage.storage_api as storage
import tasktracker.lib.permissions as permissions
import tasktracker.lib.models as models
from tasktracker.lib.custom_exceptions import RightsValidationException


class Service:
	
	"""
	The class represents operations that can be performed on all types of entities:tasks, groups,plans and users
	
	The inner class Operation represents string names for all of the operations defined.
	
	There is a handler for each of the operations, that can be set in the attribute 'handlers' of dictionary type:
	{operation_name, function}
	
	Class also gives an ability to specify basic and particular permissions for the user to perform operations
	on different objects. This can be achived by decorating a handler for the operation with specified permissions function, so that
	an exception will be raised if permissions for the user, attempting to perform operation, are denied.
	
	
	For example, if you want to edit status and description of specific task on behalf of some user, when task's id is 3 and user is known,
	then use the following syntax:
	
	self.edit(user, Task, 3, {Operation.CHANGE_TASK_STATUS: Task.Status.DONE, Operation.CHANGE_TASK_DESCRIPTION: 'new deescription'})
	
	
	Also you may want to assign tasks with ids 7,8,9 to the person, whose id is 5, so that he will become their executor,
	then you should write the following:
	
	self.change_relashionships(current_user, User, 5, Task, [7,8,9], Operation.ASSIGN_TASKS)
	
	
	If you want to delete the tasks with ids 11,12,13 on behalf of current_user, so you need to use the following exspression:
	
	self.perform_operation(current_user, Task, [11,12,13], Operation.DELETE_TASK)
	
	
	You can use the following operations with edit method:
	CHANGE_TASK_START_DATETIME,
	CHANGE_TASK_FINISH_DATETIME,
	CHANGE_PRIORITY,
	CHANGE_TASK_STATUS,
	CHANGE_TASK_DESCRIPTION,
	CHANGE_GROUP_NAME,
	CHANGE_TASK_NAME,
	CHANGE_PLAN_NAME,
	CHANGE_PLAN_PERIOD
	
	The following operations can be applied by means of change_relashionships method
	
	ADD_USER_TO_GROUP, in order add multiple users to a group
	REMOVE_USER_FROM_GROUP, in order remove multiple users from a group
	ADD_TASK_TO_GROUP, in order to add a task to multiple groups
	REMOVE_TASK_FROM_GROUP, in order to remove a task from multiple groups
	ASSIGN_TASKS, in order to assign multiple tasks to a certain user
	ADD_SUBTASKS, in order to add existing tasks to become subtasks of another task
	
	And you can use method perform_operation with the following operations:
	DELETE_GROUP
	DELETE_TASK
	ARCHIVE_TASK
	DELETE_PLAN
	DELETE_USER
	LEVEL_UP_TASK
	"""

	class Operation:
		ADD_USER_TO_GROUP = 'add_user_to_group'
		REMOVE_USER_FROM_GROUP = 'remove_user_from_group'
		ADD_TASK_TO_GROUP = 'add_task_to_group'
		REMOVE_TASK_FROM_GROUP = 'remove_task_from_group'
		DELETE_GROUP = 'delete_group'
		DELETE_TASK = 'delete_task'
		ARCHIVE_TASK = 'archive_task'
		DELETE_PLAN = 'delete_plan'
		DELETE_USER = 'delete_user'
		LEVEL_UP_TASK = 'level_up_task'
		CHANGE_TASK_START_DATETIME = 'change_task_start_datetime'
		CHANGE_TASK_FINISH_DATETIME = 'change_task_finish_datetime'
		ASSIGN_TASKS = 'change_task_executor'
		ADD_SUBTASKS = 'change_task_parent'
		CHANGE_PRIORITY = 'change_task_priority'
		CHANGE_TASK_STATUS = 'change_task_status'
		CHANGE_TASK_DESCRIPTION = 'change_task_description'
		CHANGE_GROUP_NAME = 'change_group_name'
		CHANGE_TASK_NAME = 'change_task_name'
		CHANGE_PLAN_NAME = 'change_plan_name'
		CHANGE_PLAN_PERIOD = 'change_plan_periodicity'
	
	str_attr_enum_map = {
		Operation.CHANGE_PRIORITY: models.Task.Priority,
	    Operation.CHANGE_TASK_STATUS: models.Task.Status,
	    Operation.CHANGE_PLAN_PERIOD: models.Plan.Period
	}
	
	def __init__(self):
		self._set_handlers()
		self._set_basic_permissions()
		self._set_particular_permissions()

	
	def _set_handlers(self):
		self.handlers = {
			self.Operation.ADD_USER_TO_GROUP: group_manager.add_user_to_group,
			self.Operation.REMOVE_USER_FROM_GROUP: group_manager.remove_user_from_group,
			self.Operation.CHANGE_GROUP_NAME: lambda group, name: storage.update(group, name=name),
			self.Operation.CHANGE_TASK_NAME: lambda task, name: storage.update(task, name=name),
		    self.Operation.CHANGE_TASK_START_DATETIME: task_manager.change_start_date_time,
			self.Operation.CHANGE_TASK_FINISH_DATETIME: task_manager.change_finish_date_time,
		    self.Operation.ASSIGN_TASKS: task_manager.change_task_executor,
		    self.Operation.ADD_SUBTASKS: task_manager.change_task_parent,
		    self.Operation.CHANGE_PRIORITY: lambda task, priority: storage.update(task, priority=priority),
		    self.Operation.CHANGE_TASK_STATUS: task_manager.set_task_status,
		    self.Operation.CHANGE_TASK_DESCRIPTION: lambda task, description: storage.update(task, description=description),
		    self.Operation.ADD_TASK_TO_GROUP: task_manager.add_task_to_group,
		    self.Operation.REMOVE_TASK_FROM_GROUP: task_manager.remove_task_from_group,
			self.Operation.CHANGE_PLAN_NAME: lambda plan, name: storage.update(plan, name=name),
			self.Operation.CHANGE_PLAN_PERIOD: plan_manager.change_period,
			self.Operation.DELETE_TASK: task_manager.delete_task,
			self.Operation.DELETE_GROUP: lambda group: storage.delete_entities(group),
			self.Operation.DELETE_PLAN: plan_manager.delete_plan,
			self.Operation.ARCHIVE_TASK: task_manager.archive_task,
			self.Operation.LEVEL_UP_TASK: task_manager.level_up_task,
			self.Operation.DELETE_USER: user_manager.delete_user}
	
	def create_task(self, user, name, operations=None):
		with storage.start_transaction():
			task = task_manager.create_task(name, user)
			
			if operations is not None:
				self.edit(user, models.Task, task.id, operations)
			
			storage.on_commit_perform(lambda: task_manager.set_creation_datetime(task.id))
		
		return task
	
	def create_user(cls, name):
		return user_manager.create_user(name)
		
	def create_plan(self, user, name, start_date, period, operations=None):
		"""
		Create a new plan
		If operations are specified, edit the first task created by plan
		Set creation date of the first task created by plan
		Roll back if task editing was not successfull
		
		:param user: user, attempting to create a plan
		:param name: plan name
		:param start_date: the date when the first task, created by plan should be started
		:param period: amount of time, that defines the difference between start dates of two tasks, created by plan
		:param operations: operations to perform on the first created task
		:return: created plan
		"""
		
		with storage.start_transaction():
			first_task = plan_manager.create_plan(name, start_date, period, user)
			
			if operations is not None:
				self.edit(user, models.Task, first_task.id, operations)
			
			storage.on_commit_perform(lambda: task_manager.set_creation_datetime(first_task.id))
			
		return first_task.planner
	
	def create_group(self, user, name):
		with storage.start_transaction():
			group = group_manager.create_group(name, user)
		
		return group
	
	def change_relashionships(self, user, target_class, target_id, entities_class, entities_ids, operation):
		
		"""
		Change relashionships between target object and each of entities.
		If type of target or type of entities is Task, change modification date and time of each task.
		Roll back if exception was raised on some stage
		
		:param user: user, attempting to change the relashionships between target and entities
		:param target_class: type of the target
		:param target_id: id of the target
		:param entities_class: type of entities
		:param entities_ids: ids of entities
		:param operation: the name of operation to be performed
		
		"""
		
		with storage.start_transaction():
			target = storage.select_for_update(target_class, target_id)
			
			for id in entities_ids:
				with storage.start_transaction():
					entity = storage.select_for_update(entities_class, id)
					
					handler = self.get_permissions_decorated_handler(user, operation)
					handler(target, entity)
					
					def update_modification_date():
						if target_class is models.Task:
							task_manager.update_modification_datetime(target)
						if entities_class is models.Task:
							task_manager.update_modification_datetime(entity)
							
						storage.on_commit_perform(update_modification_date)

	def perform_operation(self, user, target_type, targets_ids, operation):
		
		"""
		Perform operation on each of the given objects, if it is allowed for user.
		If type of objects is Task change modification datetime of each task.
		Roll back if exception was raised on some stage
		
		:param user: the user, attempting to perform operation
		:param target_type: the class of objects, that will be edited
		:param targets_ids: ids of objects that will be edited
		:param operation: the name of unary operation present in handlers
		
		"""
		
		with storage.start_transaction():
			for id in targets_ids:
				with storage.start_transaction():
					target = storage.select_for_update(target_type, id)
					
					handler = self.get_permissions_decorated_handler(user, operation)
					handler(target)
					
					if target_type is models.Task and operation!= self.Operation.DELETE_TASK:
							storage.on_commit_perform(lambda: task_manager.update_modification_datetime(target))
					
	def edit(self, user, target_type, target_id, operations):
		
		"""
		Set each property of target to the corresponding value using defined handlers, if it is allowed for user.
		If type of the object if Task, change it's modification datetime.
		Roll back if exception was raised on some stage
		
		:param user: the user, attempting to edit object
		:param target_type: type of the object to be edited
		:param target_id: id of the object to be edited
		:param operations: operations that will be performed while target editing
		:type operations: dict of type {operation_name: new_value_to_be_set_on_target}
		"""
		
		with storage.start_transaction():
			target = storage.select_for_update(target_type, target_id)
			
			for operation, value in operations.items():
				handler = self.get_permissions_decorated_handler(user, operation)
				handler(target, value)
			
			if target_type is models.Task:
				storage.on_commit_perform(lambda: task_manager.update_modification_datetime(target))

	def get_permissions_decorated_handler(self, user, operation):
		
		"""
		:param user: user, attempting to perform operation
		:param operation: the name of operation
		
		:return: new handler for the operation, decorated according to basic and specific rights of operation
		"""
		
		initial_handler = self.handlers[operation]
		permissions_decorator = self.get_permissions_decorator(user, operation)
		return permissions_decorator(initial_handler)

	def get_permissions_decorator(self, user, operation):
		
		"""
		:param user: user, attempting to perform operation
		:param operation: the name of operation
		
		:return: decorator for the handler, mapped with operation. It raises exception if basic or specific rights of the user to perform operation with
		the given arguments are violated
		"""
		
		def decorator(func):
			def new_func(target, *args):
				
				self.__raise_if_permissions_denied(
					self.particular_permissions_validations,
				    operation,
				    user,
				    target,
				    *args
				)
				
				self.__raise_if_basic_permissions_denied(operation, user, target)

				func(target, *args)
		
			return new_func
		return decorator
	
	def __raise_if_basic_permissions_denied(self, operation, user, target):
		
		"""
		:param operation: operation to check rights on
		:param user: user, attempting to perform operation
		:param target: target of the operation
		
		Check basic rights of user to modify target if operation is not mentioned in specific rights or the need of checking basic rights is marked as true
		in specific rights dictionary
		"""
		
		if (operation in self.particular_permissions_validations and
			not self.particular_permissions_validations[operation].include_basic_permissions):
			return
		
		self.__raise_if_permissions_denied(
			self.basic_permissions_validations,
		    type(target).__name__,
		    user,
		    target
		)
		
	def __raise_if_permissions_denied(self, permissions_handlers_dict, key, user, target, *args):
		
		"""
		:param permissions_handlers_dict: dictionary of type {key: (rights function, error message)}
		:param key: key to the rights_dict
		:param user: user, attempting to perform operation
		:param target: target of the operation
		:param args: additional arguments needed to perform operation
		
		:raise RightsValidationException: if rights function, specified for the defined key returns False
		"""
		
		if key in permissions_handlers_dict:
			permissions_tuple = permissions_handlers_dict[key]
			permission_handler = permissions_tuple.permissions_function
			message = permissions_tuple.error_message
			if not permission_handler(user, target, *args):
				if len(args) == 1:
					args = args[0]
				message = message.format(user=user.id, target=target.id, value=args)
				raise RightsValidationException(message)
	
	def _set_basic_permissions(self):
		
		"""
		Set basic permissions for user to perform operations on objects of type Task,Group,Plan,User
		
		Create a dictionary of type
		{name of the entity class : (permissions function, error message if rights are violated)}

		The signature of rights function is func(user, target), returns True if the user has rights to modify the target, otherwise returns False
		"""
		
		basic_task_permissions_error_mes = 'User {user} has no rights to edit task {target}'
		basic_group_permissions_error_mes = 'User {user} has no rights to edit group {target}'
		basic_plan_permissions_error_mes = 'Only executor of plan {target} can edit it'
		basic_user_permissions_error_mes = 'Only user himself can edit his account'
		
		BasicPermissions = collections.namedtuple('BasicPermissions', 'permissions_function error_message')
		
		
		self.basic_permissions_validations = {
			models.Task.__name__: BasicPermissions(
				permissions.basic_task_permissions,
		        basic_task_permissions_error_mes
			),
			models.Group.__name__: BasicPermissions(
				permissions.basic_group_permissions,
		        basic_group_permissions_error_mes
			),
            models.Plan.__name__: BasicPermissions(
	            permissions.executor_only_permissions,
		        basic_plan_permissions_error_mes
            ),
            models.User.__name__: BasicPermissions(
	            permissions.edit_user_self_only_permissions,
		        basic_user_permissions_error_mes
            )
		}
	
	def _set_particular_permissions(self):
		
		"""
		Set permissions for user to perform particular operations
		
		Create dictionary of type
		{name of operation : (rights function, error message if rights are violated, should check basic rights)}
		
		The signature of rights function is the same as the signature of operation's handler.
		Rights function returns True if the user has rights to perform operation with the given arguments, otherwise returns False
		"""
		
		ParticularPermissions = collections.namedtuple('ParticularPermissions', 'permissions_function error_message include_basic_permissions')
		
		self.particular_permissions_validations = {}
		change_executor_error_mes = 'User {user} has no rights change executor of task {value} to {target}'
		change_status_error_mes = 'User {user} has no rights to change status of task {target}. Only executor is allowed.'
		add_subtasks_error_mes = 'User {user} has no rights to modify task {value}'
		add_task_to_group_error_mes = 'User {user} has no rights to add task {target} to group {value}'
		delete_group_error_mes = 'It is forbidden to delete default group'
		add_user_to_group_error_mes = 'It is forbidden to add users to default group'
		remove_task_from_group_error_mes = 'User {user} can not modify group {value}'

		self.particular_permissions_validations = {
				self.Operation.ADD_TASK_TO_GROUP:ParticularPermissions(
					permissions.add_task_to_group_permissions,
                    add_task_to_group_error_mes,
                    False
				),
				self.Operation.CHANGE_TASK_STATUS:ParticularPermissions(
					permissions.executor_only_permissions,
                    change_status_error_mes,
                    False
				),
				self.Operation.ADD_USER_TO_GROUP:ParticularPermissions(
					permissions.not_user_default_group_permissions,
                    add_user_to_group_error_mes,
                    True
				),
				self.Operation.DELETE_GROUP:ParticularPermissions(
					permissions.not_user_default_group_permissions,
                    delete_group_error_mes,
                    True
				),
				self.Operation.ADD_SUBTASKS:ParticularPermissions(
					permissions.add_subtasks_permissions,
                    add_subtasks_error_mes,
                    True
				),
				self.Operation.ASSIGN_TASKS:ParticularPermissions(
					permissions.assign_tasks_permissions,
                    change_executor_error_mes,
                    False
				),
				self.Operation.REMOVE_TASK_FROM_GROUP:ParticularPermissions(
					permissions.remove_task_from_group_permissions,
                    remove_task_from_group_error_mes,
                    True
				)
			}


			


		