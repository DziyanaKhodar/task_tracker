from datetime import timedelta
from dateutil.relativedelta import relativedelta

from tasktracker.lib.models import Plan


def shift_date_by_period(initial_date, period):
    if initial_date is None:
        return None
    date = initial_date
    if period is Plan.Period.DAYLY:
        date = initial_date + timedelta(days=1)
    elif period is Plan.Period.WEEKLY:
        date = initial_date + timedelta(days=7)
    elif period is Plan.Period.MONTHLY:
        date = initial_date + relativedelta(months=1)
    elif period is Plan.Period.YEARLY:
        date = initial_date + relativedelta(months=12)

    return date
