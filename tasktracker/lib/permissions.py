import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.managers.task_manager import check_ancestor_is_group_member
from tasktracker.lib.models import Task


def add_task_to_group_permissions(user, task, group):
	return basic_group_permissions(user, group) and executor_only_permissions(user, task) and not check_ancestor_is_group_member(group, task)


def basic_task_permissions(user, task):
	if task.archived:
		return False
	
	user_groups = user.groups.all()
	for user_group in user_groups:
		if user_group in task.groups.all() or check_ancestor_is_group_member(user_group, task):
			return True
	
	if task.is_periodic():
		return executor_only_permissions(user, task)
	return False


def remove_task_from_group_permissions(user, task, group):
	return basic_group_permissions(user, group)


def basic_group_permissions(user, group):
	return group in user.groups.all()


def assign_tasks_permissions(user, another_user, task):
	if not basic_task_permissions(user, task):
		return False
	
	return storage.check_task_in_user_groups(task.id, another_user.id)


def add_subtasks_permissions(user, new_parent, task):
	return basic_task_permissions(user, task)


def not_user_default_group_permissions(user, group, *args):
	return group != user.default_group


def executor_only_permissions(user, target, *args):
	is_executor = target.user == user
	if type(target) is Task and target.archived:
		return False
	return is_executor


def edit_user_self_only_permissions(user, target_user):
	return user == target_user


