"""
Module represents operations on groups.

There is an ability to create new groups.
Add and remove users from group.
"""

import tasktracker.lib.models as models
import tasktracker.lib.custom_exceptions as exceptions
import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.logger import get_logger


logger = get_logger()


def add_user_to_group(group, user_to_add):
	
	"""
	Add user to the given group.
	
	:param group
	:param user_to_add
	
	:raise GroupMembershipException: if user_to_add is already a member of group
	"""
	
	logger.debug('add_user_to_group was called: group: {}, user: {}'.format(group.id, user_to_add.id))
	
	if user_to_add in group.users.all():
		raise exceptions.GroupMembershipException(
			'User {} is already a member of group {}'
		    .format(user_to_add.id, group.id)
		)
	
	group.users.add(user_to_add)


def remove_user_from_group(group, user_to_remove):
	
	"""
	Remove user from the given group.
	Remove all the user's task from the given group.
	
	:param group
	:param user_to_remove
	
	:raise GroupMembershipException: if user_to_remove is not a member of group
	:raise NotAllowedActionException: if user_to_remove is the only member of the group
	"""
	
	logger.debug(
		'remove_user_from_group was called: group: {}, user: {}'
	    .format(group.id, user_to_remove.id)
	)
	
	if not group in user_to_remove.groups.all():
		raise exceptions.GroupMembershipException(
			"User {} is not a member of group {}"
		    .format(user_to_remove.id, group.id)
		)
		
	if group.users.count() == 1:
		raise exceptions.NotAllowedActionException(
			"User {} is the only member of group {}, delete group instead"
			.format(user_to_remove.id, group.id)
		)

	group.users.remove(user_to_remove)
	
	user_group_tasks = storage.get_group_executor_tasks(group.id, user_to_remove.id)
	for task in user_group_tasks:
		group.tasks.remove(task)


def create_group(name, user):
	
	"""
	Create a new group and add user as the first member.
	
	:param name: name of the new group
	:param user: user that creates the group
	"""
	
	logger.debug('create_group is called: user: {}, name: {}'.format(user.id, name))
	
	group = models.Group(name=name)
	storage.save_entity(group)
	user.groups.add(group)
	return group

