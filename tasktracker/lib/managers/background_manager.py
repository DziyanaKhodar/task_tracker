"""Module represents operations, that will be run on the background with no user interaction."""


import time
from django.utils import timezone

import tasktracker.lib.models as models
import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.managers.task_manager import set_task_status
from tasktracker.lib.managers.plan_manager import (
	check_need_create_new_task,
	create_next_task
)


def create_planned_tasks():
	
	"""For all plans check if a new task should be created and create one if should."""
	
	plans = storage.get_all(models.Plan)
	for plan in plans:
		with storage.start_transaction():
			plan = storage.select_for_update_with_related(
				models.Plan,
				plan.id,
				'last_created_task'
			)
			if check_need_create_new_task(plan):
				create_next_task(plan)
	
		
def update_statuses():
	
	"""For all tasks, whose finish datetime is less than now, mark as failed"""
	
	tasks = storage.get_all_not_archived_unfinished_tasks()
	for task in tasks:
		finish_datetime = task.finish_datetime
		if timezone.now() >= finish_datetime:
			set_task_status(task, models.Task.Status.FAILED)

			
def run_background_processing(sleep_time):
	
	while True:
		storage.close_old_connections()
		create_planned_tasks()
		update_statuses()
		time.sleep(sleep_time)

