"""
Module represents operations on users.
There is an ability to create and delete users.

"""

import tasktracker.lib.models as models
import tasktracker.lib.custom_exceptions as exceptions
import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.logger import get_logger
from tasktracker import configuration as config


logger = get_logger()


def create_user(name):
	
	"""
	Create a user with the given name.
	Create a group, which will contain all user's root tasks.
	Return created user
	
	:raise UserAlreadyExistsException: if a user with the same name already exists.
	Create a new user with the defined name.
	"""
	
	
	logger.debug('create_user was called: name: {}'.format(name))
	
	if storage.check_user_exists(name):
		raise exceptions.NotAllowedActionException('User {} already exists'.format(name))
	else:
		group = models.Group(name=config.DEFAULT_GROUP_NAME)
		storage.save_entity(group)
		new_user = models.User(id=name, default_group=group)
		storage.save_entity(new_user)
		new_user.groups.add(group)

		return new_user


def delete_user(user_to_delete):
	
	"""
	Delete the user from storage.
	
	Delete all the groups, where the defined user is the only member.
	Delete all user's plans and tasks
	"""

	logger.debug('delete_user was called: user: {}'.format(user_to_delete.id))

	for group in user_to_delete.groups.all():
			if group.users.count() == 1:
				storage.delete_entities(group)
				
	storage.delete_entities(user_to_delete.plans.all())
	storage.delete_entities(user_to_delete.tasks.all())
	storage.delete_entities(user_to_delete)

