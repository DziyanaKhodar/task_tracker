"""
Module represents operations on plans.

There is an ability to create and delete plans,
check if a new task should be created according to the plan,
create new tasks, change the period of tasks creation

"""

from django.utils import timezone

import tasktracker.lib.datetime_extensions as datetime_extensions
import tasktracker.lib.storage.storage_api as storage
import tasktracker.lib.models as models
import tasktracker.lib.managers.task_manager
from tasktracker import configuration as config
from tasktracker.lib.logger import get_logger


logger = get_logger()


def delete_plan(plan):
	
	"""
	Delete the plan.
	Add all the tasks, created according to the plan, that are not archived yet to the default group of plan's executor.
	"""
	
	logger.debug('delete_plan was called: plan: {}'.format(plan.id))
	
	for task in plan.tasks.all():
		plan.user.default_group.tasks.add(task)
	
	storage.delete_entities(plan)


def create_plan(name, start_datetime, period, user):
	
	"""
	Create a new plan and the first task of this plan with the defined name, start date and executor
	
	:param name: the name of a new plan and default name of all tasks that will be created according to the new plan
	:param start_date: the date when the first task created according to the plan should be started
	:type start_date: datetime.date
	:param period: periodicity with which new tasks will be created
	:type period: Plan.Period
	:param user: executor of all the tasks, that will be created according to the plan
	

	"""
	
	logger.debug('create_plan was called: name: {}, start_date: {}, periodicity: {}, user: {}'
	             .format(name, start_datetime, period.name, user.id))
	
	first_task = models.Task(name=name, user=user)
	tasktracker.lib.managers.task_manager.change_start_date_time(first_task, start_datetime)
	planner = models.Plan(name=name, user=user, period=period)
	storage.save_entity(first_task)
	storage.save_entity(planner)
	__bind_task_plan(first_task, planner)
	
	return first_task


def __bind_task_plan(task, plan):
	task.planner = plan
	storage.save_entity(task)
	plan.last_created_task = task
	storage.save_entity(plan)


def create_next_task(plan):
	
	"""
	Create new task according to the plan
	
	Set the new task's name and executor to the plan's ones.
	
	Calculate task's time limitations using the last task, created according to the plan
	"""
	
	logger.debug('create_next_task was called: plan: {}'.format(plan.id))
	
	last_created_task = plan.last_created_task
	task = models.Task(name=plan.name)
	
	task.start_datetime = datetime_extensions.shift_date_by_period(last_created_task.start_datetime, plan.period)
	task.finish_datetime = datetime_extensions.shift_date_by_period(last_created_task.finish_datetime, plan.period)

	task.user = plan.user
	__bind_task_plan(task, plan)

	
def check_need_create_new_task(plan):
	
	"""
	Check if a new task should be created according to the plan
	
	Calculate the date and time when the next task should be started
	And compare the interval between now and the calculated datetime with the one,
	specified in configuration
	
	"""
	task_creation_interval_before_start = config.TASK_CREATION_INTERVAL_BEFORE_START[plan.period]
	
	last_created_task = plan.last_created_task
	task_start_datetime = last_created_task.start_datetime
	new_task_start_datetime = datetime_extensions.shift_date_by_period(task_start_datetime, plan.period)
	
	now = timezone.now()
	
	time_before_new_task_start = new_task_start_datetime - now
	return time_before_new_task_start <= task_creation_interval_before_start


def change_period(plan, period):
	
	"""
	Change the interval between creating new tasks. Create new task according to new period.
	
	:param plan: plan to edit
	:param period: new period of tasks creation
	:type period: Plan.Period
	"""
	
	logger.debug('change_periodicity was called: plan: {}, period: {}'.format(plan.id, period.name))
	
	if period == plan.period:
		return
	
	plan.period = period
	create_next_task(plan)

