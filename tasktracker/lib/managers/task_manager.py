"""
The module represents operations that can be performed on tasks.

There is a possibility to change task\'s properties such as time limitations, status of task execution;
set update and modification time of a task.

Also the module gives an opportunity to change task\'s relashionship with the other entity:
an ability to add and remove tasks from groups, change the user that executes task, edit tasks tree by adding subtasks
and moving tasks one level higher in the tasks tree.

Finally, there is an ability to create a task, delete it and mark as archived.
"""
from collections import namedtuple
from datetime import timedelta
from django.utils import timezone

import tasktracker.lib.models as models
import tasktracker.lib.custom_exceptions as exceptions
import tasktracker.lib.managers.plan_manager
import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.logger import get_logger


logger = get_logger()

TreeNode = namedtuple('TreeNode', 'nesting task')

def get_task_tree_for_group(group):
	container = []
	group_tasks = group.tasks.all()
	for task in group_tasks:
		container.extend(get_task_tree(task))
	
	return container

def get_task_tree(task):
	container = []
	__build_task_tree(task, 0, container)
	return container

def __build_task_tree(task, nesting, container):
	container.append(TreeNode(nesting, task))
	children = task.children.all()
	
	if len(children) == 0:
		return
	
	for child in children:
		__build_task_tree(child, nesting + 1, container)
	

def check_ancestor_is_group_member(group, task):
	
	"""
	:return true if the defined group contains a task,
	that is an ansector of the given one.
	Otherwise return false.
	"""
	
	parent = task.parent_task
	while parent is not None:
		if parent in group.tasks.all():
			return parent
		parent = parent.parent_task
	return False

def is_ancestor(first_task, second_task):
	
	"""
	:return true if second is an ancestor of first in the tasks tree.
	Otherwise return false.
	"""
	
	parent = first_task.parent_task
	while parent is not None:
		if second_task == parent:
			return True
		parent = parent.parent_task
	return False


def get_subtree_nodes_in_group(group, task):
	
	"""
	Return an iterator for all tasks that are
	both members of group and present in subtree of task.
	If task doesn't have any subtasks it will be the only result returned by the iterator.
	
	:param task: root task of tasks subtree
	:param group: group to investigate
	"""
	
	all_nodes = task.get_all_subtree_nodes()
	for node in all_nodes:
		if node in group.tasks.all():
			yield node

def change_task_executor(new_user, task):
	
	"""
	For all tasks in the tasks tree with task being the root change executor to new_user.
	For all the groups of each task check if it contains new_user as a member and delete the task from it if not.
	Add task to the new_user's default_group.
	
	:param new_user: a user, that will be set as a task's executor
	:param task: task to edit
	
	:raise NotAllowedActionException: if the task has a parent task
	"""
	
	logger.debug('change_task_executor is called: user: {}, task: {}'.format(new_user.id, task.id))
	
	if task.user == new_user:
		return
	
	if task.parent_task is not None:  # we can not change the user of subtask and leave the user of parent task the same
		raise exceptions.NotAllowedActionException(
			"it is not allowed to change user of a subtask. reassign the executor of task {} first"
				.format(task.parent_task.id)
		)

	tree_nodes = task.get_all_subtree_nodes()

	for node in tree_nodes:
		node.user = new_user###### do this if node's user is old user
		
		node_groups = node.groups.all()
		for group in node_groups:
			
			if new_user not in group.users.all():  # when we change task's executor we delete the task from all personal groups of old_user and from all groups that are shared and new user is not in it
				group.tasks.remove(node)
		
		storage.save_entity(node)

	new_user.default_group.tasks.add(task)


def add_task_to_group(task, group):
	
	"""
	Remove from group the tasks which are present in the subtree where task is root.
	Add task to group.
	
	:raise NotAllowedActionException: if task is periodic(is created according to the plan)
	:raise GroupMembershipException: if task is already a member of the group
	:raise AncestorException: if ancestor of the task is a member of the group
	"""
	
	logger.debug('add_task_to_group is called: task: {}, group: {}'.format(task.id, group.id))
	
	if task.is_periodic():
		raise exceptions.NotAllowedActionException("Periodic task {} can not be added to any group".format(task.id))
	
	if group in task.groups.all():
		raise exceptions.GroupMembershipException(
			"Group {} already contains task {}".format(
				group.id, task.id
			)
		)
	
	ancestor = check_ancestor_is_group_member(group, task)
	if ancestor:
		raise exceptions.AncestorException(
			"An ancestor of task {} is already in the group {} "
				.format(task.id, group.id)
		)
	
	for child_in_group in get_subtree_nodes_in_group(group, task):
		group.tasks.remove(child_in_group)
	
	group.tasks.add(task)


def remove_task_from_group(task, group):
	"""
	If the group is some user's default group, then delete task.
	Remove task from group.
	
	:raise GroupMembershipException: if the task is not a member of the group
	"""
	logger.debug('remove_task_from_group is called: task: {}, group: {}'.format(task.id, group.id))
	
	if task not in group.tasks.all():
		raise exceptions.GroupMembershipException("Task {} is not in a group {}".format(task.id, group.id))
	
	if group == task.user.default_group:
		delete_task(task)
		return
	
	group.tasks.remove(task)

def level_up_task(task):
	"""
	Make the task a subtask of it's parent's parent task.
	Add task to all groups of it's previous parent.
	
	:raise NotAllowedActionException: if task does not have parent task
	"""
	
	logger.debug('level_up is called: task: {}'.format(task.id))
	
	if task.parent_task is not None:
		old_parent = task.parent_task
		new_parent = old_parent.parent_task
		task.parent_task = new_parent
		old_parent_groups = old_parent.groups.all()
		
		for group in old_parent_groups:
			group.tasks.add(task)
		storage.save_entity(task)
	else:
		raise exceptions.NotAllowedActionException('Impossible to level up, task {} does not have a parent task'.format(task.id))


def change_task_parent(new_parent, task):
	"""
	Set task's parent task in tasks tree to new_parent.
	Change task's executor to new_parent's executor.
	Remove task from all groups, where both task and new_parent(or it's ancestor) are present.
	
	:param task: task to edit
	:param new_parent: new parent task of task
	
	:raise AncestorException: if task is an ancestor of new_parent
	:raise NotAllowedActionException if task is periodic

	"""
	logger.debug('change_task_parent is called: new_parent: {}, task: {}'.format(new_parent.id, task.id))
	
	if new_parent == task.parent_task or new_parent == task:
		return
	
	if is_ancestor(new_parent, task):
		raise exceptions.AncestorException('Task {} is ancestor of task {}'.format(task.id, new_parent.id))
		
	if task.is_periodic():
		raise exceptions.NotAllowedActionException("Cannot set a parent of a periodic task {}".format(task.id))

	task.parent_task = None
	change_task_executor(new_parent.user, task)###### don't do this
	task.parent_task = new_parent


	for task_group in task.groups.all():
		if check_ancestor_is_group_member(task_group, task):  # we remove our task from all groups, where it was and new parent or new ancestor was
			task_group.tasks.remove(task)
	storage.save_entity(task)


def change_start_date_time(task, start_datetime):
	"""
	Set the date and time of the given task to start_datetime
	If task's deadline is not set, then define it one day later at the same time
	
	:raise TimeLimitationsIncorrectException: if the time of task's beggining is later than it's deadline
	"""
	if task.finish_datetime is None:
		task.finish_datetime = start_datetime + timedelta(days=1)
		
	if start_datetime > task.finish_datetime:
		raise exceptions.TimeLimitationsIncorrectException('Date and time of task\'s start must be less or equal to it\'s deadline')
	
	task.start_datetime = start_datetime
	
	storage.save_entity(task)
	
	
def change_finish_date_time(task, finish_datetime):
	"""
	Set the date and time of task's deadline to finish_datetime
	If task's start date and time are not defined then set it to one day earlier at the same time
	
	:raise TimeLimitationsIncorrectException: if the date and time of task's beginning is later that it's deadline
	"""
	if task.start_datetime is None:
		task.start_datetime = finish_datetime - timedelta(days=1)
	
	if task.start_datetime > finish_datetime:
		raise exceptions.TimeLimitationsIncorrectException('Date and time of task\'s start must be less or equal to it\'s deadline')
	
	task.finish_datetime = finish_datetime
	
	storage.save_entity(task)
	

def set_task_status(task, status):
	"""
	Archive task if status is 'done'.
	Change the statuses of all tasks in subtree with task being a root to status, if status is either 'done' or 'failed'
	
	:param task: task to edit
	:param status: status to be set
	:type status: Task.Status
	"""
	logger.debug('change_status is called: task: {}, status: {}'.format(task.id, status.name))
	
	task.status = status

	if status == models.Task.Status.DONE or status == models.Task.Status.FAILED:
		if status == models.Task.Status.DONE:
			archive_task(task)
			
		for node in task.get_all_subtree_nodes():
			node.status = status
			storage.save_entity(node)
	storage.save_entity(task)


def __create_next_task_if_periodic(task):
	"""
	If task is periodic and it is the last task, created according to the plan,
	create next according to the plan
	"""
	logger.debug('manage_removal is called: task: {}'.format(task.id))
	
	if task.is_periodic() and task.planner.last_created_task == task:
		tasktracker.lib.managers.plan_manager.create_next_task(task.planner)
		task.plan = None
	
	
def delete_task(task):
	"""
	Delete all tasks in subtree with task being a root.
	If task is periodic, then create a new task according to the plan.
	"""
	logger.debug('delete_task is called: task: {}'.format(task.id))
	
	__create_next_task_if_periodic(task)
	for node in task.get_all_subtree_nodes():
		storage.delete_entities(node)
	
	
	
def archive_task(task):
	"""
	Mark all the tasks in subtree with task being a root as archived.
	If task is periodic, then create a new task according to the plan.
	
	Remove references to parent_task, plan anf groups
	
	:raise AlreadyArchivedException: if task is already marked as archived
	"""
	logger.debug('archive_task is called: task: {}'.format(task.id))
	__create_next_task_if_periodic(task)
	
	if task.archived:
		raise exceptions.NotAllowedActionException('Task {} is already archived'.format(task.id))
	for node in task.get_all_subtree_nodes():
		node.archived = True
		task.parent_task = None
		task.planner = None
		task.groups.set([])
		storage.save_entity(node)


def create_task(name, user):
	"""
	Create a new task with the given name.
	Set task's executor to user.
	Add new task to user's default group
	"""
	logger.debug('create_task is called: user: {}, name: {}'.format(user.id, name))
	
	task = models.Task(name=name)
	task.user = user
	storage.save_entity(task)
	user.default_group.tasks.add(task)

	return task


def update_modification_datetime(task):
	"""Set task's modification time to now."""
	logger.debug('update_modification_datetime is called: task: {}'.format(task.id))
	
	now = timezone.now()
	
	task.modified_datetime = now
	storage.save_entity(task)

	
def set_creation_datetime(task_id):
	"""	Set task's modification and creation date to now."""
	logger.debug('set_creation_datetime is called: task: {}'.format(task_id))
	
	task = storage.get_by_id(models.Task, task_id)
	
	now = timezone.now()
	
	task.creation_datetime = now
	task.modification_datetime = now
	if task.start_datetime is None:
		change_start_date_time(task, now)
	storage.save_entity(task)


	




