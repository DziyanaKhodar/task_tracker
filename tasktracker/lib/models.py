from django.db import models

from enum import Enum
from collections import deque

class ChoiceEnum(Enum):
	
	@classmethod
	def choices(cls):
		return tuple((tag.name, tag.value) for tag in cls)

class Group(models.Model):
	
	name = models.CharField(max_length=250)
	tasks = models.ManyToManyField('Task', related_name='groups')
	
	def __str__(self):
		return '[{} {}]'.format(self.id, self.name)


class Task(models.Model):
	
	class Status(ChoiceEnum):
		NOTSTARTED = 'Not started'
		STARTED = 'Started'
		INPROCESS = 'In progress'
		DONE = 'Done'
		FAILED = 'Failed'
	
	class Priority(ChoiceEnum):
		HIGH = 'High'
		AVERAGE = 'Average'
		LOW = 'Low'
	
	parent_task = models.ForeignKey(to='Task', related_name='children', on_delete=models.SET_NULL, null=True)
	name = models.CharField(max_length=250)
	description = models.CharField(max_length=2000, null=True)
	db_priority = models.CharField(
		max_length=30,
		choices=Priority.choices(),
		default=Priority.AVERAGE.name)
	db_status = models.CharField(
		max_length=30,
		choices=Status.choices(),
		default=Status.NOTSTARTED.name)
	
	start_datetime = models.DateTimeField(null=True)
	finish_datetime = models.DateTimeField(null=True)
	creation_datetime = models.DateTimeField(null=True)
	modification_datetime = models.DateTimeField(null=True)
	user = models.ForeignKey('User', related_name='tasks', on_delete=models.SET_NULL, null=True)
	planner = models.ForeignKey(to='Plan', related_name='tasks', on_delete=models.SET_NULL, null=True)
	archived = models.BooleanField(default=False)
	

	@property
	def status(self):
		return self.Status[self.db_status]
	
	@status.setter
	def status(self, status):
		self.db_status = status.name
	
	@property
	def priority(self):
		return self.Priority[self.db_priority]
	
	@priority.setter
	def priority(self, priority):
		self.db_priority = priority.name
	
	@property
	def human_readable_status(self):
		return self.get_db_status_display()
	
	@property
	def human_readable_priority(self):
		return self.get_db_priority_display()
	
	def __str__(self):
		return '[{} {}]'.format(self.id, self.name)
	
	def is_periodic(self):
		return self.planner is not None
	
	def get_all_subtree_nodes(self):
		"""
		:return: iterator for all tasks in task tree, where self is root
		"""
		queue = deque()
		queue.append(self)
		while not len(queue) == 0:
			qtask = queue.popleft()
			yield qtask
			
			for subtask in qtask.children.all():
				queue.append(subtask)


class User(models.Model):
	id = models.CharField(max_length=100, primary_key=True)
	groups = models.ManyToManyField("Group", related_name='users')
	default_group = models.OneToOneField("Group", on_delete=models.SET_NULL, related_name="owner", null=True)
	
	def __str__(self):
		return "[{}]".format(self.id)


class Plan(models.Model):
	
	class Period(ChoiceEnum):
		DAYLY = 'Dayly'
		WEEKLY = 'Weekly'
		MONTHLY = 'Monthly'
		YEARLY = 'Yearly'
	
	last_created_task = models.ForeignKey("Task", on_delete=models.SET_NULL, null=True)
	db_period = models.CharField(
		max_length=30,
		choices=Period.choices())
	name = models.CharField(max_length=250, null=True)
	user = models.ForeignKey('User', related_name='plans', on_delete=models.SET_NULL, null=True)
	
	@property
	def period(self):
		return self.Period[self.db_period]
	
	@period.setter
	def period(self, value):
		self.db_period = value.name
	
	@property
	def human_readable_period(self):
		return self.get_db_period_display()
	
	def __str__(self):
		return '[{} {}]'.format(self.id, self.name)



