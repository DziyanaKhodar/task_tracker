class AncestorException(Exception):
    pass


class GroupMembershipException(Exception):
    pass


class NotAllowedActionException(Exception):
    pass


class TimeLimitationsIncorrectException(Exception):
    pass


class RightsValidationException(Exception):
    pass


class TaskNotFoundException(Exception):
    pass


class GroupNotFoundException(Exception):
    pass


class UserNotFoundException(Exception):
    pass


class PlanNotFoundException(Exception):
    pass

   