import argparse
import sys
from datetime import datetime

from tasktracker.lib.models import Plan
from tasktracker import configuration as config
from tasktracker.lib.service import Service
from tasktracker.lib.models import Task
from tasktracker.console.displayer import display_message


class DefaultHelpParser(argparse.ArgumentParser):
	def error(self, message):
		display_message(message)
		self.print_help()
		sys.exit(2)
		

class ParserBuilder:

	def __init__(self):
		self.date_pattern = config.DATETIME_SETTINGS['DATE_PATTERN']
		self.datetime_pattern = config.DATETIME_SETTINGS['DATETIME_PATTERN']
			
	def parsed_datetime(self, str_datetime):
		try:
			return datetime.strptime(str_datetime, self.datetime_pattern)
		except:
			return datetime.strptime(str_datetime, self.date_pattern)
		
	def store_to_dest_dict(self, arg_name):
		class StoreToDict(argparse.Action):
			def __call__(self, parser, namespace, values, option_strings=None):
				if getattr(namespace, self.dest) is None:
					setattr(namespace, self.dest, {})
				dict = getattr(namespace, self.dest)
				dict[arg_name] = values
		
		return StoreToDict
	
	def store_name_to_dest(self, name):
		class StoreNameToDest(argparse.Action):
			def __call__(self, parser, namespace, values, option_strings=None):
				setattr(namespace, self.dest, name)
				
		return StoreNameToDest
	
	def store_name_to_dest_value_to_name(self, name):
		class StoreNameToDestValueToName(argparse.Action):
			def __call__(self, parser, namespace, values, option_string=None):
				setattr(namespace, self.dest, name)
				setattr(namespace, name, values)
			
		return StoreNameToDestValueToName

	def create_parser(self):
		parser = DefaultHelpParser(add_help=False)
		
		
		targets_subparsers = parser.add_subparsers(dest='target', parser_class=DefaultHelpParser)
		targets_subparsers.required = True
		
		base_task_parser = argparse.ArgumentParser(add_help=False)
		self.__build_base_task_parser(base_task_parser)
		
		task_parser = targets_subparsers.add_parser('task', help='Working with tasks', add_help=False)
		self.__build_task_parser(task_parser, base_task_parser)
		
		plan_parser = targets_subparsers.add_parser('plan', help='Working with periodic plan', add_help=False)
		self.__build_plan_parser(plan_parser, base_task_parser)
		
		group_parser = targets_subparsers.add_parser('group', help='Working with groups', add_help=False)
		self.__build_group_parser(group_parser)
		
		user_parser = targets_subparsers.add_parser('user', help='Working with users', add_help=False)
		self.__build_user_parser(user_parser)
		
		bgprocess_parser = targets_subparsers.add_parser(
			'bgprocess',
		    help='Working with background processes',
		    add_help=False
		)
		self.__build_bgprocess_parser(bgprocess_parser)
		
		db_parser = targets_subparsers.add_parser('db', help='Working with databases')
		self.__build_db_parser(db_parser)
		
		return parser
	
	def __build_db_parser(self, db_parser):
		db_subparsers = db_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		db_subparsers.required = True
		
		db_parser = DefaultHelpParser(add_help=False)
		db_parser.add_argument('name', type=str, help='Define the name of database')
		db_parser.add_argument(
			'-un',
		    '--user-name',
            required=True,
		    dest='dbuser_name',
		    type=str,
		    help='Define the user of mysql'
		)
		db_parser.add_argument('-p', '--password', type=str, help='Define the password for mysql')
		db_parser.add_argument('--host', type=str, help='Define the host for mysql')
		db_parser.add_argument('--port', type=str, help='Define the port for mysql')
		
		create_parser = db_subparsers.add_parser('create', help='Create a new database for task tracker', parents=[db_parser])
		
		use_parser = db_subparsers.add_parser('use', help='Defiine which database should be used, you can change the decision any time')
		use_subparsers = use_parser.add_subparsers(dest='type', parser_class=DefaultHelpParser)
		use_subparsers.required = True
		
		unknown_parser = use_subparsers.add_parser('unknown', help='Specifyy the database that exists but not present in aliases', parents=[db_parser])
		
		alias_parser = use_subparsers.add_parser('alias', help='Specify an existing alias for the database, so that the database will be used for further actions')
		alias_parser.add_argument('alias', type=str, help='Enter the alias fr database')
		
		remove_alias_parser = db_subparsers.add_parser('remove-alias', help='Remove alias of the database, so that you can use it explicitly by defining mysql user name and password')
		remove_alias_parser.add_argument('alias', type=str, help='Define the database alias to be removed')
		
		add_alias_parser = db_subparsers.add_parser('add-alias', help='Add an alias for some database of task tracker so that you will no longer have to specify mysql user name and password', parents=[db_parser])
		add_alias_parser.add_argument('alias', type=str, help='Choose an alias - a short word that you will be able to use if you want to change database to the following one.')
		
		show_parser = db_subparsers.add_parser('show', help='See availiable aliases or current database name')
		exclusive_group = show_parser.add_mutually_exclusive_group()
		exclusive_group.add_argument(
			'-a',
		    '--all',
		    action=self.store_name_to_dest('all'),
		    dest='type',
		    nargs=0,
		    help='See all aliases that are taken and what database each of them corresponds'
		)
		exclusive_group.add_argument(
			'-c',
		    '--current',
		    action=self.store_name_to_dest('current'),
		    dest='type',
		    nargs=0,
		    help='See the name of current database being used'
		)
		
	def __build_bgprocess_parser(self, bgprocess_parser):
		bgprocess_subparsers = bgprocess_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		bgprocess_subparsers.required = True
		
		bgprocess_subparsers.add_parser('start', help='Start the background process')
		bgprocess_subparsers.add_parser('stop', help='Stop background process')
		bgprocess_subparsers.add_parser('restart', help='Restart background process')
		bgprocess_subparsers.add_parser('running', help='Check if background process is running')
	
	def __build_user_parser(self, user_parser):
		user_subparsers = user_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		user_subparsers.required = True
		
		add_user_parser = user_subparsers.add_parser('add', help='Create a new user')
		add_user_parser.add_argument('name', type=str, help='Specify the name of a new user')
		
		delete_user_parser = user_subparsers.add_parser('delete', help='Delete the user')
		delete_user_parser.add_argument('id', type=str, help='Define the name of the user you would like to delete')

		user_subparsers.add_parser('current', help='Show the name of current user')
		
		assign_parser = user_subparsers.add_parser('assign', help='Assign chosen tasks to the user')
		assign_parser.add_argument('id', type=str, help='Specify user id')
		assign_parser.add_argument('tasks', type=int, nargs='+', help='Specify tasks ids')
		
		logout_parser = user_subparsers.add_parser('logout', help='Logout')
		
		login_user_parser = user_subparsers.add_parser('login', help='Login and start working with your tasks')
		login_user_parser.add_argument('name', type=str, help='Specify your name in order to login')
		
	def __build_base_task_parser(self, base_task_parser):
		base_task_parser.add_argument(
			'-pr',
			'--priority',
			type=str,
			choices=[tag.name for tag in Task.Priority],
			action=self.store_to_dest_dict(Service.Operation.CHANGE_PRIORITY),
			dest='operations',
			help='Attach priority to a task, default is AVERAGE',
		    metavar=''
		)
		
		base_task_parser.add_argument(
			'-s', '--status',
			type=str,
			action=self.store_to_dest_dict(Service.Operation.CHANGE_TASK_STATUS),
			dest='operations',
			choices=[tag.name for tag in Task.Status],
			help='Specify status of a task, default is NOTSTARTED',
		    metavar=''
		)
		
		base_task_parser.add_argument(
			'-d',
			'--description',
			type=str,
			action=self.store_to_dest_dict(Service.Operation.CHANGE_TASK_DESCRIPTION),
			dest='operations',
			help='Specify full description of a task',
		    metavar=''
		)
		
		self._build_time_limitations(base_task_parser)
	
	def _build_time_limitations(self, base_task_parser):
		base_task_parser.add_argument(
			'-st',
			'--start-time',
			type=self.parsed_datetime,
			dest='operations',
			action=self.store_to_dest_dict(Service.Operation.CHANGE_TASK_START_DATETIME),
			help='Specify the date when the task should be started',
		    metavar=''
		)

		base_task_parser.add_argument(
			'-ft',
			'--finish-time',
			type=self.parsed_datetime,
			dest='operations',
			action=self.store_to_dest_dict(Service.Operation.CHANGE_TASK_FINISH_DATETIME),
			help='Specify the date when the task should be finished, use the following format ',
		    metavar=''
		)

	def __build_edit_task_parser(self, edit_task_parser):
		edit_task_parser.add_argument(
			'id',
		    type=int,
		    help='Define the id of the task you would like to edit'
		)
		
		edit_task_parser.add_argument(
			'-n',
		    '--name',
		    dest='operations',
		    type=str,
		    action=self.store_to_dest_dict(Service.Operation.CHANGE_TASK_NAME),
			help='Change the name of a task',
			metavar=''
		)

	def __build_task_parser(self, task_parser, base_task_parser):
		task_subparsers = task_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		task_subparsers.required = True
		
		add_task_parser = task_subparsers.add_parser('add', parents=[base_task_parser])
		add_task_parser.add_argument('name', help='Specify the name of a new task')
		
		level_up_parser = task_subparsers.add_parser('level-up', help='Raise chosen tasks one level higher in the task tree')
		level_up_parser.add_argument(
			'ids',
		    type=int,
		    nargs='+',
		    help='Define the ids of task you would like to raise one level higher in the task tree'
		)
		
		add_subtasks_parser = task_subparsers.add_parser('add-subtasks', help='Make existing tasks subtasks of the following task')
		add_subtasks_parser.add_argument('id', type=int, help='Specify the id of parent task')
		add_subtasks_parser.add_argument(
			'tasks',
		    type=int,
		    nargs='+',
		    help='Specify the ids of tasks that will become subtasks'
		)
		
		groups_parser = argparse.ArgumentParser(add_help=False)
		groups_parser.add_argument('id', type=int, help='Define task id')
		groups_parser.add_argument('groups', type=int, nargs='+', help='Define ids of the groups')
		
		add_to_group_parser = task_subparsers.add_parser(
			'add-to-groups',
		    help='Add task to groups',
		    parents=[groups_parser]
		)
		
		remove_from_groups_parser = task_subparsers.add_parser(
			'remove-from-groups',
		    help='Remove task from defined groups',
		    parents=[groups_parser]
		)
		
		edit_task_parser = task_subparsers.add_parser('edit', parents=[base_task_parser])
		self.__build_edit_task_parser(edit_task_parser)
		
		delete_task_parser = task_subparsers.add_parser('delete', help='Delete tasks')
		delete_task_parser.add_argument(
			'ids',
		    type=int,
		    nargs='+',
			help='Define the ids of the tasks you would like to delete'
		)
		
		archive_task_parser = task_subparsers.add_parser('archive', help='Archive tasks')
		archive_task_parser.add_argument(
			'ids',
		    type=int,
		    nargs='+',
			help='Define the ids of tasks you would like to archive'
		)
		
		show_task_parser = task_subparsers.add_parser('show', help='Show information about task')
		exclusive_group = show_task_parser.add_mutually_exclusive_group()
		
		exclusive_group.add_argument(
			'--id',
		    type=int,
		    action=self.store_name_to_dest_value_to_name('id'),
		    dest='type',
		    help='Specify the id of a task you would like to look through'
		)
		
		exclusive_group.add_argument(
			'--all',
		    action=self.store_name_to_dest('all'),
		    dest='type',
		    nargs=0,
		    help='Show all tasks and plans'
		)
		
		exclusive_group.add_argument(
			'--archived',
		    action=self.store_name_to_dest('archived'),
		    dest='type',
		    nargs=0,
		    help='Show all archived tasks'
		)
	
	def __build_plan_parser(self, plan_parser, base_task_parser):
		plan_subparsers = plan_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		plan_subparsers.required = True
		
		add_plan_parser = plan_subparsers.add_parser(
			'add',
		    parents=[base_task_parser],
			help='Create a plan for periodic tasks and specify properties of the first created task')
		self.__build_add_plan_parser(add_plan_parser)
		
		edit_plan_parser = plan_subparsers.add_parser('edit', help='Edit the properties of a plan')
		self.__build_edit_plan_parser(edit_plan_parser)
		
		delete_plan_parser = plan_subparsers.add_parser('delete', help='Delete plan')
		
		delete_plan_parser.add_argument(
			'ids',
		    type=int,
		    nargs='+',
		    help='Define the id of plan you would like to delete'
		)
	
		show_plan_parser = plan_subparsers.add_parser('show', help='Show information about plan and its up to date tasks')
		exclusive_group = show_plan_parser.add_mutually_exclusive_group()
		
		exclusive_group.add_argument(
			'--id',
            type=int,
            action=self.store_name_to_dest_value_to_name('id'),
            dest='type',
            help='Specify the id of the plan you would like to look through'
		)
		
		exclusive_group.add_argument(
			'--all',
            action=self.store_name_to_dest('all'),
            dest='type',
            nargs=0,
            help='Show all plans of current user'
		)
		

	def __build_add_plan_parser(self, add_plan_parser):
		add_plan_parser.add_argument(
			'name',
		    type=str,
			help='Specify the default name of all periodic tasks, that will be created according to the plan'
		)
		
		add_plan_parser.add_argument(
			'start_datetime',
		    type=self.parsed_datetime,
			help='Specify the start date of first task that will be created according to the plan'
		)
		
		add_plan_parser.add_argument(
			'period',
		    type=str,
			help='Define the periodicity of tasks creation'
		)
	
	def __build_edit_plan_parser(self, edit_plan_parser):
		edit_plan_parser.add_argument('id', type=int, help='Define the id of plan you would like to edit')
		
		edit_plan_parser.add_argument(
			'-n',
		    '--name',
		    type=str,
		    action=self.store_to_dest_dict(Service.Operation.CHANGE_PLAN_NAME),
			dest='operations',
			help='Change the default name of all periodic tasks that will be created',
		    metavar=''
		)
		
		edit_plan_parser.add_argument(
			'-p',
		    '--period',
		    type=str,
			choices=[tag.name for tag in Plan.Period],
			action=self.store_to_dest_dict(Service.Operation.CHANGE_PLAN_PERIOD),
			dest='operations',
			help='Change the periodicity of tasks creation',
		    metavar=''
		)

	def __build_group_parser(self, group_parser):
		group_subparsers = group_parser.add_subparsers(dest='command', parser_class=DefaultHelpParser)
		group_subparsers.required = True
		add_group_parser = group_subparsers.add_parser('add', help='Create a new group')
		add_group_parser.add_argument('name', type=str, help='Specify the name of a new group')
		
		edit_group_parser = group_subparsers.add_parser('edit',	help='Edit the chosen group')
		
		edit_group_parser.add_argument(
			'id',
		    type=int,
		    help='Define the id of the group you would like to edit'
		)
		
		edit_group_parser.add_argument(
			'-n',
			'--name',
            type=str,
            dest='operations',
            metavar='',
            action=self.store_to_dest_dict(Service.Operation.CHANGE_GROUP_NAME),
            help='Change the name of the chosen group'
		)
		
		delete_group_parser = group_subparsers.add_parser('delete', help='Delete group')
		delete_group_parser.add_argument(
			'ids',
		    type=int,
		    nargs='+',
		    help='Define ids of the groups you would like to delete'
		)
		
		group_show_parser = group_subparsers.add_parser('show', help='Show information about the group')
		exclusive_group = group_show_parser.add_mutually_exclusive_group()
		
		exclusive_group.add_argument(
			'--id',
		    type=int,
		    action=self.store_name_to_dest_value_to_name('id'),
		    dest='type',
		    help='Display information about the group, it\'s users and tasks'
		)
		
		exclusive_group.add_argument(
			'--all',
		    action=self.store_name_to_dest('all'),
            dest='type',
            nargs=0,
            help='Display all groups of the current user'
		)
		
		users_parser = argparse.ArgumentParser(add_help=False)
		users_parser.add_argument('id', type=int, help='Define the group id')
		users_parser.add_argument(
			'users',
            type=str,
            nargs='+',
            help='Define ids of the users'
		)
		
		add_users_parser = group_subparsers.add_parser('add-users', help='Add users to the chosen group', parents=[users_parser])
		remove_users_parser = group_subparsers.add_parser('remove-users', help='Remove users from the chosen group', parents=[users_parser])
		
