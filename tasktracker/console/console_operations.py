import os

import tasktracker.lib.storage.storage_api as storage
import tasktracker.lib.models as models
import tasktracker.lib.custom_exceptions as custom_exceptions
import tasktracker.console.displayer as displayer
import tasktracker.lib.permissions as permissions
import tasktracker.configuration as config

from tasktracker.lib.service import Service
from tasktracker.lib.managers.background_manager import run_background_processing
from tasktracker.lib.background_process import BackgroundProcess
from tasktracker.lib.logger import (
	catch_log_exception,
	get_logger
)


logger = get_logger()


def auth_func_decorator(func):
	def new_func(self, *args):
		"""
		Log an error message if func is called when no user has authorized
		
		"""
		
		if not self.current_user:
			logger.error('Action cannot be performed without authorization')
		else:
			func(self, *args)
	
	return new_func


def authorization_validation(cls):
	"""
	Decorate all methods that do not start with '_',but end with '_require_auth'
	so that they cannot be run, if no user has authorized
	"""
	
	class_attrs = dir(cls)
	for attr in class_attrs:
		value = getattr(cls, attr)
		if callable(value):
			if not attr.startswith('_') and attr.endswith('require_auth'):
					new_value = auth_func_decorator(value)
					setattr(cls, attr, new_value)
	return cls


@authorization_validation
class ConsoleOperations:
	"""
	The class represents an ability to work with all types of objects on behalf of the current user.
	
	It is possible to login and logout,
	see information about tasks, groups and plans,
	run and stop background process,
	perform the operations on all types of entities
	
	All exceptions, raised during handling of an operation are caught and logged
	
	All methods, that require authorization, have names that  end with _require_auth.
	These methods will not be executed if self.current_user is not set.
	
	"""
	
	def __init__(self, db_manager):
		
		self.bgprocess = BackgroundProcess(config.BACKGROUND_SETTINGS['PID_PATH'])
		self.db_manager = db_manager
		self.service = Service()
		
		self.current_user_path = config.CURRENT_USER_PATH
		self.current_user = None
		self.__set_curerent_user()
	
	def __set_curerent_user(self):
		
		"""
		Read the current user name from predefined file and set self.current_user
		to the one, that was found in db.
		If there is invalid name in the file, or the file is absent,
		set self.current_user to None and log about the problem with file
		If the file is absent, create a new one
		"""
		
		if not os.path.exists(self.current_user_path):
			logger.debug('File with current user id was not found. New file is created.')
			f = open(self.current_user_path, 'x').close()
			self.current_user = None
			return
		
		with open(self.current_user_path, 'r+') as f:
			name = f.read()
			try:
				self.current_user = storage.get_user_by_id(name)
			except custom_exceptions.UserNotFoundException as err:
				logger.debug('File with current user id contains invalid id, current user is not set')
				f.truncate(0)
			except Exception as ex:
				logger.error('Unable to resolve current user')
				logger.error(str(ex))
	
	@catch_log_exception('Failed to create a new task.')
	def add_task_require_auth(self, name, task_operations):
		task = self.service.create_task(self.current_user,
		                         name,
		                         task_operations)
		displayer.display_task_short(task)
		
	
	@catch_log_exception('Failed to edit the task')
	def edit_task_require_auth(self, task_id, task_operations):
		self.service.edit(
			self.current_user,
		    models.Task,
		    task_id,
		    task_operations
		)
		
	@catch_log_exception('Failed to increase tasks\' level in the tasks tree')
	def level_up_tasks_require_auth(self, task_ids):
		self.service.perform_operation(
			self.current_user,
		    models.Task,
		    task_ids,
		    Service.Operation.LEVEL_UP_TASK
		)
	
	@catch_log_exception('Failed to delete tasks')
	def delete_tasks_require_auth(self, task_ids):
		self.service.perform_operation(
			self.current_user,
		    models.Task,
		    task_ids,
		    Service.Operation.DELETE_TASK
		)
	
	@catch_log_exception('Failed to archive tasks')
	def archive_tasks_require_auth(self, task_ids):
		self.service.perform_operation(
			self.current_user,
		    models.Task,
		    task_ids,
		    Service.Operation.ARCHIVE_TASK
		)
		
	@catch_log_exception('Failed to add task to groups')
	def add_task_to_groups_require_auth(self, task_id, groups_ids):
		self.service.change_relashionships(
			self.current_user,
		    models.Task,
		    task_id,
		    models.Group,
		    groups_ids,
		    Service.Operation.ADD_TASK_TO_GROUP
		)
		
	@catch_log_exception('Failed to remove task from groups')
	def remove_task_from_groups_require_auth(self, task_id, groups_id):
		self.service.change_relashionships(
			self.current_user,
		    models.Task,
		    task_id,
            models.Group,
            groups_id,
            Service.Operation.REMOVE_TASK_FROM_GROUP
		)
	
	@catch_log_exception('Unable to show task')
	def show_task_require_auth(self, task_id):
		logger.debug('User {} attempts to see information about task {}'
		             .format(self.current_user.id, task_id))
		task = storage.get_by_id(models.Task, task_id)
		
		if not permissions.basic_task_permissions(self.current_user, task):
			logger.error('User {} has no rights to see information about task {}'
			             .format(self.current_user.id, task_id))
			return
		
		displayer.display_task_full(task)
	
	@catch_log_exception('Unable to show tasks')
	def show_all_tasks_require_auth(self):
		tasks = storage.get_not_archived_user_tasks(self.current_user.id)
		displayer.display_all(tasks, displayer.display_task_short)
		plans = self.current_user.plans.all()
		displayer.display_all(plans, displayer.display_plan)
		
	@catch_log_exception('Unable to show archived tasks')
	def show_all_archived_tasks_require_auth(self):
		tasks = storage.get_all_archived_user_tasks(self.current_user.id)
		displayer.display_all(tasks, displayer.display_task_short)
	
	@catch_log_exception('Failed to assign tasks')
	def assign_tasks_require_auth(self, user_id, tasks_ids):
		self.service.change_relashionships(
			self.current_user,
		    models.User, user_id,
		    models.Task, tasks_ids,
		    Service.Operation.ASSIGN_TASKS
		)
	
	@catch_log_exception('Failed to add subtasks')
	def add_subtasks_require_auth(self, parent_id, children_ids):
		self.service.change_relashionships(
			self.current_user,
		    models.Task,
		    parent_id,
		    models.Task,
		    children_ids,
		    Service.Operation.ADD_SUBTASKS
		)
	
	@catch_log_exception('Failed to add users to group')
	def add_users_to_group_require_auth(self, group_id, users_ids):
		self.service.change_relashionships(
			self.current_user,
		    models.Group, group_id,
		    models.User,
		    users_ids,
		    Service.Operation.ADD_USER_TO_GROUP
		)
		
	@catch_log_exception('Failed to remove users from group')
	def remove_users_from_group_require_auth(self, group_id, users_ids):
		self.service.change_relashionships(
			self.current_user,
		    models.Group,
		    group_id, models.User,
		    users_ids,
		    Service.Operation.REMOVE_USER_FROM_GROUP
		)
	
	@catch_log_exception('Unable to add group')
	def add_group_require_auth(self, name):
		group = self.service.create_group(self.current_user, name)
		displayer.display_group_short(group)
	
	@catch_log_exception('Failed to edit group')
	def edit_group_require_auth(self, group_id, group_operations):
		self.service.edit(
			self.current_user,
		    models.Group,
		    group_id,
		    group_operations
		)
		
	@catch_log_exception('Failed to delete groups')
	def delete_groups_require_auth(self, group_ids):
		self.service.perform_operation(
			self.current_user,
		    models.Group,
		    group_ids,
		    Service.Operation.DELETE_GROUP
		)
		
	@catch_log_exception('Unable to show group')
	def show_group_require_auth(self, group_id):
		logger.debug('User {} attempts to see information about group {}'
		             .format(self.current_user.id, group_id))
		
		group = storage.get_by_id(models.Group, group_id)
		
		if not permissions.basic_group_permissions(self.current_user, group):
			displayer.display_message('You have no rights to see information about group {}'
			                          .format(group.id))
			logger.debug('Rights verification failed')
			return
		
		displayer.display_group_full(group)
	
	@catch_log_exception('Unable to show groups')
	def show_groups_require_auth(self):
		groups = self.current_user.groups.all()
		displayer.display_all(groups, displayer.display_group_short)

	@catch_log_exception('Unable to create plan')
	def add_plan_require_auth(self, name, start_date, period, plan_operations):
		plan = self.service.create_plan(
			self.current_user,
		    name,
		    start_date,
		    period,
		    plan_operations
		)
		displayer.display_plan(plan)
	
	@catch_log_exception('Failed to edit plan')
	def edit_plan_require_auth(self, plan_id, plan_operations):
		self.service.edit(
			self.current_user,
		    models.Plan,
		    plan_id,
			plan_operations
		)
	
	@catch_log_exception('Failed to delete plans')
	def delete_plans_require_auth(self, plan_ids):
		self.service.perform_operation(
			self.current_user,
		    models.Plan,
		    plan_ids,
		    Service.Operation.DELETE_PLAN
		)
		
	@catch_log_exception('Unable to show plan')
	def show_plan_require_auth(self, plan_id):
		logger.debug('User {} attempts to see information about plan {}'
		             .format(self.current_user.id, plan_id))
		plan = storage.get_by_id(models.Plan, plan_id)
		
		if not permissions.executor_only_permissions(self.current_user, plan):
			logger.error('User {} has no rights to see information about plan {}'
			             .format(self.current_user.id, plan_id))
			return
		
		displayer.display_plan(plan)
		
	@catch_log_exception('Unable to show plans')
	def show_plans_require_auth(self):
		plans = self.current_user.plans.all()
		displayer.display_all(plans, displayer.display_plan)

	@catch_log_exception('Failed to add user')
	def add_user(self, name):
		self.service.create_user(name)

	@catch_log_exception('Failed to delete user')
	def delete_user_require_auth(self, name):
		self.service.perform_operation(
			self.current_user,
		    models.User,
		    [name],
		    Service.Operation.DELETE_USER
		)
	
	def show_current_user(self):
		displayer.display_message(self.current_user)
	
	@catch_log_exception('Login failed')
	def login(self, name):
		"""
		If current user is not set, try to get a user from db, according to the given name.
		If that succeded then write the name to the predefined file for current user's
		"""
		if self.current_user is not None:
			displayer.display_message('You should logout first.')
			logger.debug('Attempt to login as user {} when user {} has not logged out'
			             .format(name, self.current_user.id))
			return
		
		self.current_user = storage.get_user_by_id(name)
		with open(self.current_user_path, 'w') as f:
			f.write(name)
		logger.debug('User {} has logged in'.format(self.current_user.id))

	@catch_log_exception('Unable to logout')
	def logout_require_auth(self):
		"""
		If current user is set, clear the predefined file with current user name
		"""
		
		if self.current_user is None:
			logger.debug('Attempt to logout when was not authorized')
			displayer.display_message('You should login first.')
			return
		
		with open(self.current_user_path, 'w') as f:
			f.write('')
		logger.debug('User {} successfully logged out'
		             .format(self.current_user.id))
		self.current_user = None
		
	@catch_log_exception('Starting backgroung process failed')
	def start_bgprocess(self):
		sleep_time = config.BACKGROUND_SETTINGS['BACKGROUND_PROCESSING_INTERVAL']
		self.bgprocess.start(run_background_processing, sleep_time)
	
	@catch_log_exception('Failed to stop background process')
	def stop_bgprocess(self):
		self.bgprocess.stop()
	
	def restart_bgprocess_require_auth(self):
		self.stop_bgprocess_require_auth()
		self.start_bgprocess_require_auth()
	
	@catch_log_exception('Unable to define background process state')
	def bgprocess_running(self):
		running = self.bgprocess.is_running()
		displayer.display_message(running)
	
	@catch_log_exception('Unable to create database')
	def create_db(self, name, dbuser_name, password, host, port):
		storage.create_db(name, dbuser_name, password, host, port)
		
	@catch_log_exception('Unable to change database')
	def change_current_db(self, alias):
		self.db_manager.change_current_db(alias)
		
	@catch_log_exception('Unable to change database')
	def change_current_db_from_unknown(self, name, dbuser_name, password, host, port):
		self.db_manager.change_current_db_from_unknown(name, dbuser_name, password, host, port)
		
	@catch_log_exception('Unable to add alias for the database')
	def add_db_alias(self, alias, name, dbuser_name, password, host, port):
		self.db_manager.add_db_alias(alias, name, dbuser_name, password, host, port)
		
	@catch_log_exception('Unable to delete alias for the database')
	def delete_db_alias(self, alias):
		self.db_manager.delete_db_alias(alias)
		
	@catch_log_exception('Unable to show aliases')
	def show_db_aliases_names_map(self):
		databases_info = self.db_manager.get_databases_info()
		displayer.display_databases_aliases_map(databases_info)
		
	@catch_log_exception('Unable to show current database')
	def show_current_db_name(self):
		databases_info = self.db_manager.get_databases_info()
		displayer.display_message(databases_info['CURRENT']['NAME'])
		
		
	
	
	
	
		