import json
import os

import tasktracker.lib.storage.storage_api as storage
import tasktracker.settings as settings
from tasktracker.lib.custom_exceptions import NotAllowedActionException
from tasktracker.lib.logger import get_logger


logger = get_logger()


class DatabasesManager:
	
	"""
	The class represents an ability to work with multiple task tracked databases.
	
	Information about databases is stored in a json file.
	 The file containss db info about current used db
	and aliases for the others. It is possible to add and remove alias for the database,
	replace current database with the one,
	that is present in aliases or not.
	 If file does not exist or is incorrect(can not be parsed as a valid json string), a new file is created .
	 The default database from tasktracker.settings is added to file with alias 'default' and marked as current.
	 
	"""

	def __init__(self, path):
		self.path = path
		self.__create_json_if_doesnt_exist()

	def __create_json_if_doesnt_exist(self):
		if os.path.isfile(self.path):
			return

		databases_info = {
							'CURRENT' : settings.DATABASES['default'],
		                    'ALIASES': {
			                                'default': settings.DATABASES['default']
		                               }
						 }
		
		self.set_databases_info(databases_info)

	def get_databases_info(self):
		"""
		Try to parse the content of json file, if it is not possible, then remove the file,
		create a new one with valid default content.
		
		Return parsed valid content.
		
		"""
		
		try:
			with open(self.path, 'r') as f:
				databases_info = json.load(f)
		except json.JSONDecodeError:
			logger.debug('Unable to parse json. Creating a new file.')
			os.remove(self.path)
			self.__create_json_if_doesnt_exist()
			return self.get_databases_info()
			
		return databases_info
			
	def set_databases_info(self, databases_info):
		with open(self.path, 'w+') as f:
			json.dump(databases_info, f)

	def check_alias_taken(self, alias):
		databases_info = self.get_databases_info()
		return alias in databases_info['ALIASES']

	def add_db_alias(self, alias, name, dbuser_name, password, host, port):
		"""
		Set a short name for the database, identified by the rest of arguments.
		Add that information to json file
		
		:raise NotAllowedActionError: if the alias is already taken and identifies another database
		
		"""
		
		if self.check_alias_taken(name):
			raise NotAllowedActionException('Database alias {} is already taken'.format(alias))
		
		db_info = storage.get_db_info(name, dbuser_name, password, host, port)
		databases_info = self.get_databases_info()
		databases_info['ALIASES'][alias] = db_info
		self.set_databases_info(databases_info)
		
	def delete_db_alias(self, alias):
		if not self.check_alias_taken(alias):
			raise ValueError('Database alias {} does not exist'.format(alias))
		
		databases_info = self.get_databases_info()
		del databases_info['ALIASES'][alias]
		self.set_databases_info(databases_info)
		
	def change_current_db(self, alias):
		"""
		Try to get information by alias from json file.
		If it is found, then write the information about db to 'CURRENT' section of json file
		"""
		
		if not self.check_alias_taken(alias):
			raise ValueError('Database alias {} does not exist'.format(alias))
		
		databases_info = self.get_databases_info()
		db_info = databases_info['ALIASES'][alias]
		databases_info['CURRENT'] = db_info
		
		self.set_databases_info(databases_info)

	def change_current_db_from_unknown(self, name, dbuser_name, password, host, port):
		db_info = storage.get_db_info(name, dbuser_name, password, host, port)
		databases_info = self.get_databases_info()
		databases_info['CURRENT'] = db_info
		
		self.set_databases_info(databases_info)
		
	def establish_db_connection(self):
		"""Retrieve information about current database from the json file
		and define it to be used in current session
		
		If current database info is None according to the json file, than set it to default one
		from the tasktracker.settings.
		
		"""
		
		databases_info = self.get_databases_info()
		db_info = databases_info['CURRENT']
		if db_info is None:
			databases_info['CURRENT'] = settings.DATABASES['default']
			self.set_databases_info(databases_info)
		storage.establish_db_connection(db_info)
		
		