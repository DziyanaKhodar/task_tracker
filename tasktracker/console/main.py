import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tasktracker.settings")
django.setup()

import tasktracker.lib.models as models
from tasktracker.console.console_operations import ConsoleOperations
from tasktracker.console.parser import ParserBuilder
from tasktracker.lib.logger import bootstrap_logger
from tasktracker.lib.service import Service
from tasktracker import configuration as config
from tasktracker.console.databases_manager import DatabasesManager


import tasktracker.lib.storage.storage_api as storage
import tasktracker.lib.managers.task_manager as tm


def main():
    if not os.path.isdir(config.TASKTRACKER_DIRECTORY):
        os.makedirs(config.TASKTRACKER_DIRECTORY)

    bootstrap_logger()
    db_manager = DatabasesManager(config.DATABASES_INFO_PATH)
    db_manager.establish_db_connection()
    
    co = ConsoleOperations(db_manager)

    parser = ParserBuilder().create_parser()
    namespace = parser.parse_args()
    
    if hasattr(namespace, 'period'):
        period = models.Plan.Period[namespace.period]

    if hasattr(namespace, 'operations') and namespace.operations is not None:
        operations = namespace.operations
        
        for operation_name in Service.str_attr_enum_map.keys():
            attr_type = Service.str_attr_enum_map[operation_name]
        
            if not operation_name in operations:
                continue
            
            string_attr = operations[operation_name]
            operations[operation_name] = attr_type[string_attr]
    else:
        operations = None
    
    handlers = {'user':
                    {
                        'add': lambda : co.add_user(namespace.name),
                        'delete': lambda : co.delete_user_require_auth(namespace.id),
                        'current': co.show_current_user,
                        'assign':lambda : co.assign_tasks_require_auth(namespace.id, namespace.tasks),
                        'logout': co.logout_require_auth,
                        'login': lambda : co.login(namespace.name)
                    },
                'plan':
                    {
                        'add': lambda : co.add_plan_require_auth(namespace.name,
                                                                 namespace.start_datetime,
                                                                 period,
                                                                 operations),
                        'delete': lambda : co.delete_plans_require_auth(namespace.ids),
                        'edit' : lambda : co.edit_plan_require_auth(namespace.id, operations),
                        'show' :
                            {
                                'id': lambda : co.show_plan_require_auth(namespace.id),
                                'all': co.show_plans_require_auth
                            }
                    },
                'group' :
                    {
                        'add' : lambda : co.add_group_require_auth(namespace.name),
                        'edit' : lambda : co.edit_group_require_auth(namespace.id, operations),
                        'delete' : lambda : co.delete_groups_require_auth(namespace.ids),
                        'add-users' : lambda : co.add_users_to_group_require_auth(namespace.id, namespace.users),
                        'remove-users' : lambda : co.remove_users_from_group_require_auth(namespace.id,
                                                                                          namespace.users),
                        'show' :
                            {
                                'id': lambda : co.show_group_require_auth(namespace.id),
                                'all': co.show_groups_require_auth
                            }
                    },
                'task':
                    {
                        'add' : lambda : co.add_task_require_auth(namespace.name, operations),
                        'edit' : lambda : co.edit_task_require_auth(namespace.id, operations),
                        'delete' : lambda : co.delete_tasks_require_auth(namespace.ids),
                        'archive' : lambda : co.archive_tasks_require_auth(namespace.ids),
                        'add-subtasks': lambda : co.add_subtasks_require_auth(namespace.id, namespace.tasks),
                        'add-to-groups': lambda : co.add_task_to_groups_require_auth(namespace.id, namespace.groups),
                        'remove-from-groups': lambda : co.remove_task_from_groups_require_auth(namespace.id,
                                                                                               namespace.groups),
                        'level-up': lambda : co.level_up_tasks_require_auth(namespace.ids),
                        'show' :
                            {
                                'id':  lambda : co.show_task_require_auth(namespace.id),
                                'all': co.show_all_tasks_require_auth,
                                'archived': co.show_all_archived_tasks_require_auth
                            }
                    },
                'bgprocess':
                    {
                        'start': co.start_bgprocess,
                        'stop': co.stop_bgprocess,
                        'running': co.bgprocess_running,
                        'restart' : co.restart_bgprocess_require_auth
                    },
                'db':
                    {
                        'create': lambda: co.create_db(namespace.name,
                                                       namespace.dbuser_name,
                                                       namespace.password,
                                                       namespace.host,
                                                       namespace.port),
                        'remove-alias': lambda: co.delete_db_alias(namespace.alias),
                        'add-alias': lambda: co.add_db_alias(namespace.alias,
                                                             namespace.name,
                                                             namespace.dbuser_name,
                                                             namespace.password,
                                                             namespace.host,
                                                             namespace.port),
                        'show' :
                            {
                                'current': co.show_current_db_name,
                                'all': co.show_db_aliases_names_map
                            },
                        'use' :
                            {
                                'unknown':lambda: co.change_current_db_from_unknown(namespace.name,
                                                                                    namespace.dbuser_name,
                                                                                    namespace.password,
                                                                                    namespace.host,
                                                                                    namespace.port),
                                'alias': lambda: co.change_current_db(namespace.alias)
                            }
                    }
    }
    
    
    handler = handlers[namespace.target][namespace.command]
    if hasattr(namespace,'type'):
        handler = handler[namespace.type]
    
    handler()


if __name__ == "__main__":
     main()

        




    




