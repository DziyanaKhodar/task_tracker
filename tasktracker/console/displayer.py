"""
Module represents functions for displaying objects to console
both in short and full style
"""

import os


def display_task_short(task):
	description = 'TASK id: [{}], name: [{}], executor: [{}]'\
		.format(task.id, task.name, task.user.id)
	
	print(description)


def display_task_full(task):
	print('id:                   {}'.format(task.id))
	print('name:                 {}'.format(task.name))
	print('priority:             {}'.format(task.human_readable_priority))
	print('status:               {}'.format(task.human_readable_status))
	print('start date and time:  {}'.format(task.start_datetime))
	print('finish date and time: {}'.format(task.finish_datetime))
	print('description: {}'.format(task.description))
	print('executor:  {}'.format(task.user.id))
	
	task_groups = task.groups.all()
	print('groups:', os.linesep)
	for gr in task_groups:
		display_group_short(gr)
		print(os.linesep)


def display_subtree(task, nesting):
	
	"""
	Display task's subtree, defining the connection 'task-subtask'
	by indentation
	
	For example:
	task
		subtask
			another subtask
	another task
	
	"""
	print('     ' * nesting, end='')
	display_task_short(task)
	subtasks = task.children.all()
	for subtask in subtasks:
		display_subtree(subtask, nesting + 1)


def display_plan(plan):
	
	"""
	Display information about the plan and all tasks, created by the plan,
	that are not archived or deleted
	"""
	
	description = 'PLAN id: [{}], name: [{}], period: [{}]'\
		.format(plan.id, plan.name, plan.period.value)
	print(description)
	tasks = plan.tasks.all()
	for task in tasks:
		print('     ', end='')
		display_task_short(task)


def display_group_short(group):
	description = 'GROUP id: [{}], name: [{}]'\
		.format(group.id, group.name)
	print(description)


def display_group_full(group):
	
	"""
	Display information about the group.
	Display tasks trees for each task that is a member of the group
	"""
	
	display_group_short(group)
	print('USERS:')
	users = group.users.all()
	display_all(users, display_user)
	print('TASKS:')
	for task in group.tasks.all():
		display_subtree(task, 1)


def display_user(user):
	description = 'USER name: [{}]'.format(user.id)
	print(description)


def display_message(*args):
	print(os.linesep)
	print(*args)


def display_all(entities, func):
	for entity in entities:
		func(entity)
		
		
def display_databases_aliases_map(databases_info):
	aliases = databases_info['ALIASES']
	for alias, db_info in aliases.items():
		print('alias [{}]'.format(alias))
	
		for item_name, item in db_info.items():
			if item_name == 'ENGINE':
				continue
			
			print('[{}] : [{}]'.format(item_name, item))
			
		print(os.linesep)
	