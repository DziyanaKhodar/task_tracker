import logging
import os
import getpass
from datetime import timedelta

from tasktracker.lib.models import Plan

TASKTRACKER_DIRECTORY = os.path.join('/home', getpass.getuser(), 'tasktracker')

CURRENT_USER_PATH = os.path.join(TASKTRACKER_DIRECTORY,'curr_user_id')#'/tmp/tasktracker_curr_user_id'

DEFAULT_GROUP_NAME = 'all'

DATABASES_INFO_PATH = os.path.join(TASKTRACKER_DIRECTORY, 'curr_db_name')

DATETIME_SETTINGS={
	'DATE_PATTERN': '%Y-%m-%d',
	'TIME_PATTERN': '%H:%M',
    'DATETIME_PATTERN': '%Y-%m-%d %H:%M'
}

BACKGROUND_SETTINGS={
	'BACKGROUND_PROCESSING_INTERVAL': 3000,
	'PID_PATH': os.path.join(TASKTRACKER_DIRECTORY,'pid')
}

LOG_SETTINGS={
	'LOG_LEVEL': logging.DEBUG,
	'FILE_LOG_PATTERN': '%(asctime)s [%(levelname)s] LOG: %(message)s',
	'CONSOLE_LOG_PATTERN': '%(message)s',
	'LOG_PATH ': os.path.join(TASKTRACKER_DIRECTORY,'logs'),#'/tmp/task_tracker_logs',
	'FILE_LOGGER_NAME': 'tasktracker_file_logger',
	'LOGGER_NAME':'tasktracker_console_logger'
}

TASK_CREATION_INTERVAL_BEFORE_START={
	Plan.Period.DAYLY: timedelta(hours=5),
	Plan.Period.WEEKLY: timedelta(hours=72),
	Plan.Period.YEARLY: timedelta(hours=1440),
	Plan.Period.MONTHLY: timedelta(hours=336)
}

