from django.shortcuts import (
	render,
	redirect
)
from django.contrib.auth import (
	login,
	logout,
	authenticate
)
from django.contrib.auth.forms import (
	AuthenticationForm,
	UserCreationForm
)
from django.contrib.auth.decorators import login_required
from django.utils import timezone

import tasktracker.lib.managers.task_manager as task_manager
import tasktracker.lib.storage.storage_api as storage
from tasktracker.lib.service import Service
import pdb
import tasktracker.web_interface.main.forms as forms
import tasktracker.lib.permissions as permissions



#service = Service()

def with_service(func):
	def new_func(request, *args):
		service = Service()
		return func(request, service, *args)
	return new_func

@login_required
@with_service
def index(request, service=None):
	username = request.user.username
	group = storage.get_user_default_group(username)
	task_tree = task_manager.get_task_tree_for_group(group)
	plans = storage.get_user_plans(username)
	return render(request, 'all.html', {'group': group, 'tasks': task_tree, 'plans': plans})


@login_required
def group(request, group_id=None):
	group = storage.get_group_by_id(group_id)
	task_tree = task_manager.get_task_tree_for_group(group)
	return render(request, 'group.html', {'group': group, 'tasks': task_tree})


@with_service
def signup(request, service=None):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			user.backend = 'django.contrib.auth.backends.ModelBackend'
			login(request, user)
			service.create_user(user.username)
			return redirect('/')
	else:
		form = UserCreationForm()
	return render(request, 'registration/signup.html', {'form': form})


@login_required
def task_edit(request, task_id=None, group_id=None):
	if request.method == 'GET':
		user = storage.get_user_by_id(request.user.username)
		user_groups = user.groups.all()
		
		
		if task_id is not None:
			
			
			task = storage.get_task_by_id(task_id)
			task_groups = task.groups.all()
			
			group_choices = set()#[(group.id, group.name) for group in task_groups])
			for group in task_groups:
				if permissions.remove_task_from_group_permissions(user, task, group):
					group_choices.add((group.id, group.name))

			for group in user_groups:
				if permissions.add_task_to_group_permissions(user, task, group):
					group_choices.add((group.id, group.name))
			
			form = forms.EditTaskForm(
				initial={
					'name':task.name,
					'description':task.description,
					'start_datetime':task.start_datetime,
					'finish_datetime':task.finish_datetime,
					'status': task.status.name,
					'priority': task.priority.name,
					'groups': [group.id for group in task_groups]
				},
				choices={
					'groups': list(group_choices)
				}
			)
			
			return render(request, 'edit-task.html', { 'form':form })
	
	#group_id = request.GET.get('group_id')
	
	return redirect('/groups/{}'.format(group_id))

