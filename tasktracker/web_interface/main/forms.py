from django import forms
from tasktracker.lib.models import Task
class EditTaskForm(forms.Form):
	
	def __init__(self, initial=None, choices=None, *args, **kwargs):
		super().__init__(*args, **kwargs)
		if initial is None:
			initial = {
				'priority': 'AVERAGE',
				'status': 'NOTSTARTED'
			}
			
		for field, value in initial.items():
			self.fields[field].initial = value
			
		if choices is not None:
			for field,values in choices.items():
				self.fields[field].choices = values
		
	name = forms.CharField(max_length=250)
	description = forms.CharField(max_length=2000, required=False)
	status = forms.MultipleChoiceField(
		choices=Task.Status.choices(),
	    widget=forms.Select
	)
	priority = forms.MultipleChoiceField(
		choices=Task.Priority.choices(),
		widget=forms.Select
	)
	start_datetime = forms.DateTimeField(widget=forms.SplitDateTimeWidget)
	finish_datetime = forms.DateTimeField(widget=forms.DateTimeInput)
	groups = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)