from django import template
from tasktracker.lib.models import Task

register = template.Library()


@register.filter(name='colorize_priority')
def colorize_priority(priority):
    COLORS = {
        Task.Priority.LOW: '#90ee90',
        Task.Priority.AVERAGE: '#fffacd',
        Task.Priority.HIGH:'#FFCCCC'
    }

    return COLORS[priority]

@register.filter(name='colorize_border_by_priority')
def colorize_border_priority(priority):
    COLORS = {
        Task.Priority.LOW: '#0C5404',
        Task.Priority.AVERAGE: '#ffd700',
        Task.Priority.HIGH: '#cc1111'
    }
    
    return COLORS[priority]
    
@register.filter(name='colorize_task')
def colorize_task(task):
    if task.status == Task.Status.FAILED:
        return '#DDDDDD'
    
    return colorize_priority(task.priority)


@register.filter(name='colorize_task_border')
def colorize_task_border(task):
    if task.status == Task.Status.FAILED:
        return '#5C6576'
    
    return colorize_border_priority(task.priority)