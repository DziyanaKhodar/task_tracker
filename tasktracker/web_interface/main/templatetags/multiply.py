from django import template

register = template.Library()


@register.filter(name='multiply_margin')
def multiply(first, second):
	return '{}px'.format(first*second)