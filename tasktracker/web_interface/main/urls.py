from django.conf.urls import url
import django.contrib.auth.views as auth_views
#from django.contrib import admin
import tasktracker.web_interface.main.views as views

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^login/', auth_views.login, name='login'),
    url(r'^logout/', auth_views.logout, {'next_page': '/login/'}, name='logout'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^task/edit/(?P<group_id>\d+)/(?P<task_id>\d+)$', views.task_edit, name='task-edit'),
    url(r'^groups/(?P<group_id>\d+)$', views.group, name='group')
]