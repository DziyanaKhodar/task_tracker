from django.test import TestCase

import tasktracker.lib.managers.task_manager as tm
import tasktracker.lib.managers.user_manager as um
import tasktracker.lib.managers.group_manager as gm
import tasktracker.lib.managers.plan_manager as pm
from tasktracker.lib.models import (
	Plan,
	Task,
	User
)

import tasktracker.lib.permissions as permissions
import datetime


class PermissionsTestCase(TestCase):
	
	def setUp(self):
		self.user = um.create_user('example')
		
	def test_executor_only_permissions(self):
		#arrange
		user = User()
		plan = Plan(user=self.user)
		task = Task(user=self.user)
		
		#act, assert
		self.assertFalse(permissions.executor_only_permissions(user, plan))
		self.assertFalse(permissions.executor_only_permissions(user, task))
		
	def test_basic_task_permissions__creator__true(self):
		#arrange
		task = tm.create_task('example', self.user)
		
		#act, assert
		self.assertTrue(permissions.basic_task_permissions(self.user, task))
		
	def test_basic_task_permissions__periodic_plan_creator__true(self):
		#arrange
		example_date = datetime.date(2018, 12, 11)
		task = pm.create_plan('example', example_date, Plan.Period.MONTHLY, self.user)
		
		#act, assert
		self.assertTrue(permissions.basic_task_permissions(self.user, task))
	
	def test_basic_task_permissions__user_in_task_group__true(self):
		#arrange
		task = tm.create_task('example', self.user)
		another = um.create_user('anothe')
		group = gm.create_group('example', self.user)
		group.tasks.add(task)
		group.users.add(another)
		
		#act, assert
		self.assertTrue(permissions.basic_task_permissions(another, task))
		
	def test_basic_task_permissions__user_in_ancestor_group__true(self):
		# arrange
		task = tm.create_task('example', self.user)
		child = tm.create_task('example', self.user)
		tm.change_task_parent(task, child)
		another = um.create_user('anothe')
		group = gm.create_group('example', self.user)
		group.tasks.add(task)
		group.users.add(another)
		
		# act, assert
		self.assertTrue(permissions.basic_task_permissions(another, child))
		
	def test_basic_task_permissions__task_archived__false(self):
		#arrange
		task = tm.create_task('example', self.user)
		task.archived = True
		
		#act, assert
		self.assertFalse(permissions.basic_task_permissions(self.user, task))
		
	def test_assign_task_permissions__task_in_shared_group__true(self):
		#arrange
		first_user = um.create_user('first_user')
		task = tm.create_task('example', self.user)
		another = um.create_user('another')
		group = gm.create_group('example', self.user)
		group.users.add(another)
		group.users.add(first_user)
		group.tasks.add(task)
		
		#act, assert
		self.assertTrue(permissions.assign_tasks_permissions(first_user, another, task))
		