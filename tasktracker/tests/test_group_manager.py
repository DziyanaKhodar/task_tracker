from django.test import TestCase

import tasktracker.lib.managers.task_manager as tm
import tasktracker.lib.managers.user_manager as um
import tasktracker.lib.managers.group_manager as gm

import tasktracker.lib.custom_exceptions as exceptions


class GroupManagerTestCase(TestCase):
	
	def setUp(self):
		self.user = um.create_user('example')
		
	def test_create_group__in_users_groups(self):
		#arrange, act
		group = gm.create_group('example', self.user)
		
		#assert
		self.assertIn(group, self.user.groups.all())
		
	def test_remove_user_from_group__removed(self):
		#arrange
		group = gm.create_group('example', self.user)
		another_user = um.create_user('another')
		another_user.groups.add(group)
		
		#act
		gm.remove_user_from_group(group, another_user)
		
		#assert
		self.assertNotIn(another_user, group.users.all())
		
	def test_remove_user_from_group__not_in_group__group_membership_exception(self):
		#arrange
		group = gm.create_group('example', self.user)
		another_user = um.create_user('another')
		
		#act, assert
		with self.assertRaises(exceptions.GroupMembershipException):
			gm.remove_user_from_group(group, another_user)
		
	def test_remove_user_from_group__the_only_one__not_allowed_action_exception(self):
		#arrange
		group = gm.create_group('example', self.user)
		
		#act, assert
		with self.assertRaises(exceptions.NotAllowedActionException):
			gm.remove_user_from_group(group, self.user)
			
	def test_remove_user_from_group__users_tasks_removed(self):
		#arrange
		group = gm.create_group('example', self.user)
		another_user = um.create_user('another')
		task = tm.create_task('task', another_user)
		group.users.add(another_user)
		group.tasks.add(task)
		
		#act
		gm.remove_user_from_group(group, another_user)
		
		#assert
		self.assertNotIn(task, group.tasks.all())
		
	def test_add_user_to_group__added(self):
		#arrange
		user = um.create_user('another')
		group = gm.create_group('group', self.user)
		
		#act
		gm.add_user_to_group(group, user)
		
		#assert
		self.assertIn(group, user.groups.all())
		
	def test_add_user_to_group__already_in__group_membership_exception(self):
		#arrange
		user = um.create_user('another')
		group = gm.create_group('group', self.user)
		group.users.add(user)
		
		#act, assert
		with self.assertRaises(exceptions.GroupMembershipException):
			gm.add_user_to_group(group, user)
			
	
	