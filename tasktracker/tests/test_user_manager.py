from django.test import TestCase
from unittest.mock import patch
from unittest.mock import Mock

import tasktracker.lib.managers.user_manager as um

import tasktracker.lib.custom_exceptions as exceptions


class UserManagerTestCase(TestCase):
	
	def test_create_user__default_group_created(self):
		#arrange, act
		user = um.create_user('example')
		
		#assert
		self.assertIsNotNone(user.default_group)
		
	def test_create_user__default_group_in_users_group(self):
		#arrange, act
		user = um.create_user('example')
		
		#assert
		self.assertIn(user.default_group, user.groups.all())
		
	def test_create_user__exists__user_already_exists_exeption(self):
		#arrange
		user = um.create_user('example')
		
		#act
		with self.assertRaises(exceptions.NotAllowedActionException):
			um.create_user('example')
			
	def test_delete_user__deleted(self):
		#arrange
		user = um.create_user('example')
		delete_mock = Mock()
		
		#act
		with patch('tasktracker.lib.storage.storage_api.delete_entities', delete_mock):
			um.delete_user(user)
			
		#assert
		delete_mock.assert_called_with(user)
		
		
			
	
		