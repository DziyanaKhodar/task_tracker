from django.test import TestCase
from unittest.mock import patch
from unittest.mock import Mock

import tasktracker.lib.storage.storage_api as storage
import tasktracker.lib.managers.task_manager as tm
import tasktracker.lib.managers.user_manager as um
import tasktracker.lib.managers.group_manager as gm
from tasktracker.lib.models import (
	Plan,
	Task
)

import tasktracker.lib.custom_exceptions as exceptions


class TaskManagerTestCase(TestCase):
	def setUp(self):
		self.user = um.create_user('example')
		storage.save_entity(self.user)
	
	def test_create__in_user_default_group(self):
		#arrange, act
		task = tm.create_task('example', self.user)
		
		#assert
		self.assertIs(task.user, self.user)
		self.assertIn(task, self.user.default_group.tasks.all())
		
	def test_set_status__done__archived(self):
		#arrange
		task = tm.create_task('example', self.user)
		archive_mock = Mock()
		
		#act
		with patch('tasktracker.lib.managers.task_manager.archive_task', archive_mock):
			tm.set_task_status(task, Task.Status.DONE)
		
		#assert
		self.assertIs(task.status, Task.Status.DONE)
		archive_mock.assert_called_once_with(task)
	
	def test_set_status__done__child_done(self):
		#arrange
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		task.get_all_subtree_nodes = Mock(return_value=[task, child])
		
		#act
		tm.set_task_status(task, Task.Status.DONE)
		
		#assert
		self.assertIs(child.status, Task.Status.DONE)
	
	def test_archive__archived(self):
		#arrange
		task = tm.create_task('exmple', self.user)
		
		#act
		tm.archive_task(task)
		
		#assert
		self.assertTrue(task.archived)
	
	def test_archive__last_created_periodic__new_created(self):
		#arrange
		
		task = tm.create_task('example', self.user)
		plan = Plan(last_created_task=task)
		task.planner = plan
		create_next_periodic_mock = Mock()
		
		#act
		with patch('tasktracker.lib.managers.plan_manager.create_next_task', create_next_periodic_mock):
			tm.archive_task(task)
		
		#assert
		create_next_periodic_mock.assert_called_once_with(plan)
	
	def test_archive__child_archived(self):
		#arrange
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		task.get_all_subtree_nodes = Mock(return_value=[task, child])
		
		#act
		tm.archive_task(task)
		
		#assert
		self.assertTrue(child.archived)
	
	def test_delete__last_created_periodic__new_created(self):
		#arrange
		task = tm.create_task('example', self.user)
		plan = Plan(last_created_task=task)
		task.planner = plan
		create_next_periodic_mock = Mock()
		
		# act
		with patch('tasktracker.lib.managers.plan_manager.create_next_task', create_next_periodic_mock):
			tm.delete_task(task)
		
		# assert
		create_next_periodic_mock.assert_called_once_with(plan)
		
	def test_delete__deleted(self):
		# arrange
		task = Task()
		task.get_all_subtree_nodes = Mock(return_value=[task])
		delete_mock = Mock()
		
		# act
		with patch('tasktracker.lib.storage.storage_api.delete_entities', delete_mock):
			tm.delete_task(task)
		
		#assert
		delete_mock.assert_called_once_with(task)

	def test_delete__has_subtask__subtask_deleted(self):
		#arrange
		task = Task()
		child = Task()
		task.get_all_subtree_nodes = Mock(return_value=[task, child])
		delete_mock = Mock()
		
		#act
		with patch('tasktracker.lib.storage.storage_api.delete_entities', delete_mock):
			tm.delete_task(task)
			
		#assert
		delete_mock.assert_called_with(child)

	def test_change_task_parent__parent_changed(self):
		# arrange
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		
		# act
		tm.change_task_parent(task, child)
		
		# assert
		self.assertIs(child.parent_task, task)
		
	def test_change_task_parent__different_executor__change_executor_called(self):
		#arrange
		task = tm.create_task('example', self.user)
		another_user = um.create_user('another')
		child = tm.create_task('child', another_user)
		change_executor_mock = Mock()
		
		#act
		with patch('tasktracker.lib.managers.task_manager.change_task_executor', change_executor_mock):
			tm.change_task_parent(task, child)
		
		#assert
		change_executor_mock.assert_called_once_with(self.user, child)
		
	def test_change_task_parent__both_in_group__child_removed(self):
		#arrange
		task = tm.create_task('example', self.user)
		group = gm.create_group('group', self.user)
		child = tm.create_task('child', self.user)
		child.groups.add(group)
		task.groups.add(group)
		
		# act
		tm.change_task_parent(task, child)
		
		#assert
		self.assertNotIn(group, child.groups.all())
		
	def test_change_task_parent__new_ancestor_in_group__child_removed(self):
		#arrange
		ancestor = tm.create_task('ancestor', self.user)
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		group = gm.create_group('group', self.user)
		ancestor.groups.add(group)
		child.groups.add(group)
		task.parent_task = ancestor
		
		#act
		tm.change_task_parent(task, child)
		
		#assert
		self.assertNotIn(group, child.groups.all())
		
	def test_change_task_parent__periodic__not_allowed_exeption(self):
		#arrange
		task = Task()
		periodic_task = Task()
		periodic_task.is_periodic = Mock(return_value=True)
		
		#act, assert
		with self.assertRaises(exceptions.NotAllowedActionException):
			tm.change_task_parent(task, periodic_task)
			
	def test_change_parent_task__child_is_ancestor__ancestor_exception(self):
		#arrange
		first_task = tm.create_task('first', self.user)
		second_task = tm.create_task('third', self.user)
		ancestor_stub = Mock(return_value=True)

		#act, assert
		with self.assertRaises(exceptions.AncestorException):
			with patch('tasktracker.lib.managers.task_manager.is_ancestor', ancestor_stub):
				tm.change_task_parent(second_task, first_task)
		
		
	def test_level_up_task__parent_changed(self):
		#arrange
		ancestor = tm.create_task('ancestor', self.user)
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		task.parent_task = ancestor
		child.parent_task = task
		
		#act
		tm.level_up_task(child)
		
		#assert
		self.assertIs(child.parent_task, ancestor)
		
	def test_level_up_task__child_added_to_old_parent_groups(self):
		#arrange
		ancestor = tm.create_task('ancestor', self.user)
		task = tm.create_task('example', self.user)
		task.parent_task = ancestor
		child = tm.create_task('child', self.user)
		child.parent_task = task
		group = gm.create_group('group', self.user)
		task.groups.add(group)
		
		#act
		tm.level_up_task(child)
		
		#assert
		self.assertIn(group, child.groups.all())
		
	def test_level_up_task__no_parent__not_allowed_exception(self):
		#arrange
		task = Mock()
		task.parent_task = None
		
		#act, assert
		with self.assertRaises(exceptions.NotAllowedActionException):
			tm.level_up_task(task)
		
	def test_remove_task_from_group__in_group__removed(self):
		#arrange
		task = tm.create_task('example', self.user)
		group = gm.create_group('group', self.user)
		task.groups.add(group)
		
		#act
		tm.remove_task_from_group(task, group)
		
		#assert
		self.assertNotIn(group, task.groups.all())
		
		
	def test_remove_task_from_group__default_group__deleted(self):
		#arrange
		task = tm.create_task('example', self.user)
		delete_mock = Mock()
		
		#act
		with patch('tasktracker.lib.managers.task_manager.delete_task', delete_mock):
			tm.remove_task_from_group(task, self.user.default_group)
		
		#assert
		delete_mock.assert_called_once_with(task)
		
	def test_remove_task_from_group__not_in_group__group_membership_exception(self):
		#arrange
		task = tm.create_task('example', self.user)
		group = gm.create_group('group', self.user)
		
		#act, assert
		with self.assertRaises(exceptions.GroupMembershipException):
			tm.remove_task_from_group(task, group)
		
		
	def test_add_task_to_group__not_in_group__added(self):
		#arrange
		task = tm.create_task('example', self.user)
		group = gm.create_group('group', self.user)
		
		#act
		tm.add_task_to_group(task, group)
		
		#assert
		self.assertIn(group, task.groups.all())
		
	def test_add_task_to_group__in_group__group_membership_exception(self):
		# arrange
		task = tm.create_task('example', self.user)
		group = gm.create_group('group', self.user)
		task.groups.add(group)
		
		# act, assert
		with self.assertRaises(exceptions.GroupMembershipException):
			tm.add_task_to_group(task, group)
		
	def test_add_task_to_group__ancestor_in_group__ancestor_in_group_exception(self):
		#arrange
		parent = Mock()
		child = tm.create_task('example', self.user)
		group = Mock()
		ancestor_in_group_stub = Mock(return_value=parent)
		
		#act, assert
		with self.assertRaises(exceptions.AncestorException):
			with patch('tasktracker.lib.managers.task_manager.check_ancestor_is_group_member', ancestor_in_group_stub):
				tm.add_task_to_group(child, group)
				
	def test_add_task_to_group__periodic__not_allowed_action_exception(self):
		#arrange
		task = Mock()
		task.is_periodic = Mock(return_value=True)
		group = Mock()
		
		with self.assertRaises(exceptions.NotAllowedActionException):
			tm.add_task_to_group(task, group)
			
	def test_add_task_to_group__child_in_group__child_remove(self):
		#arrange
		task = tm.create_task('example', self.user)
		child = tm.create_task('child', self.user)
		group = gm.create_group('group', self.user)
		child.groups.add(group)
		subtree_node_in_group_stub = Mock(return_value=[child])
		
		#act
		with patch('tasktracker.lib.managers.task_manager.get_subtree_nodes_in_group', subtree_node_in_group_stub):
			tm.add_task_to_group(task, group)
			
		#assert
		self.assertNotIn(group, child.groups.all())
		
	def test_change_task_executor__changed(self):
		#arrange
		task = tm.create_task('example', self.user)
		another = um.create_user('another')
		
		#act
		tm.change_task_executor(another, task)
		
		#assert
		self.assertIs(task.user, another)
		
	def test_change_task_executor__subtask__not_allowed_action_exception(self):
		#arrange
		task = tm.create_task('example', self.user)
		task.parent_task = tm.create_task('parent', self.user)
		another_user = um.create_user('another')
		
		#act, assert
		with self.assertRaises(exceptions.NotAllowedActionException):
			tm.change_task_executor(another_user, task)
			
	def test_change_task_executor__in_new_executors_default_group(self):
		#arrange
		task = tm.create_task('example', self.user)
		another_user = um.create_user('another')
		
		#act
		tm.change_task_executor(another_user, task)
		
		#assert
		self.assertIn(task, another_user.default_group.tasks.all())
		
	def test_change_task_executor__has_subtask__subtask_executor_changed(self):
		#arrange
		task = tm.create_task('example', self.user)
		another_user = um.create_user('another')
		child = tm.create_task('child', self.user)
		task.get_all_subtree_nodes = Mock(return_value=[task, child])
		
		#act
		tm.change_task_executor(another_user, task)
		
		#assert
		self.assertIs(task.user, another_user)
		
	def test_change_task_executor__task_in_group_new_exec_not__task_removed(self):
		#arrange
		task = tm.create_task('example', self.user)
		another_user = um.create_user('another')
		group = gm.create_group('group', self.user)
		task.groups.add(group)
		task.get_all_subtree_nodes = Mock(return_value=[task])
		
		#act
		tm.change_task_executor(another_user, task)
		
		#assert
		self.assertNotIn(group, task.groups.all())
		
	def test_change_task_executor__task_in_shared_group__stays(self):
		#arrange
		task = tm.create_task('example', self.user)
		another_user = um.create_user('another')
		group = gm.create_group('group', self.user)
		task.groups.add(group)
		another_user.groups.add(group)
		task.get_all_subtree_nodes = Mock(return_value=[task])
		
		#act
		tm.change_task_executor(another_user, task)
		
		#assert
		self.assertIn(group, task.groups.all())
		
	def test_ancestor(self):
		#arrange
		first_task = Task()
		second_task = Task()
		third_task = Task()
		fourth_task = Task()
		
		first_task.parent_task = second_task
		second_task.parent_task = third_task
		third_task.parent_task = fourth_task
		
		#act
		first_second = tm.is_ancestor(first_task, second_task)
		second_third = tm.is_ancestor(second_task, third_task)
		third_fourth = tm.is_ancestor(third_task, fourth_task)
		
		#assert
		self.assertTrue(first_second)
		self.assertTrue(second_third)
		self.assertTrue(third_fourth)
		
	def test_get_all_subtree_nodes(self):
		#arrange
		first_task = tm.create_task('first', self.user)
		second_task = tm.create_task('second', self.user)
		third_task = tm.create_task('third', self.user)
		fourth_task = tm.create_task('fourth', self.user)
		fifth_task = tm.create_task('fifth', self.user)
		
		tm.change_task_parent(first_task, second_task)
		tm.change_task_parent(first_task, third_task)
		tm.change_task_parent(second_task, fourth_task)
		tm.change_task_parent(fifth_task, first_task)
		
		all_nodes = [first_task, second_task, third_task, fourth_task, fifth_task]
		
		#act
		for node in fifth_task.get_all_subtree_nodes():
			for task in all_nodes:
				if task == node:
					all_nodes.remove(task)
		
		#assert
		self.assertTrue(len(all_nodes) == 0)
		
		
		
