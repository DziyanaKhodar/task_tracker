from django.test import TestCase
from unittest.mock import patch
from unittest.mock import Mock
import datetime

import tasktracker.lib.managers.user_manager as um
import tasktracker.lib.managers.plan_manager as pm

from tasktracker.lib.models import Plan


class PlanManagerTestCase(TestCase):
	def setUp(self):
		self.user = um.create_user('example')
		
	def test_change_periodicity__create_next_periodic_called(self):
		#arrange
		plan = Plan()
		plan.period = Plan.Period.DAYLY
		create_next_periodic_mock = Mock()
		
		#act
		with patch('tasktracker.lib.managers.plan_manager.create_next_task', create_next_periodic_mock):
			pm.change_period(plan, Plan.Period.MONTHLY)
		
		#assert
		create_next_periodic_mock.assert_called_once_with(plan)
		
	def test_change_periodicity__same__create_next_periodic_not_called(self):
		# arrange
		plan = Plan()
		plan.period = Plan.Period.MONTHLY
		create_next_periodic_mock = Mock()
		
		# act
		with patch('tasktracker.lib.managers.plan_manager.create_next_task', create_next_periodic_mock):
			pm.change_period(plan, Plan.Period.MONTHLY)
		
		# assert
		create_next_periodic_mock.assert_not_called()
		
	def test_create_plan__first_task_created(self):
		#arrange, act
		example_date = datetime.datetime(2018, 12, 11)
		first_task = pm.create_plan('example', example_date, Plan.Period.MONTHLY, self.user)
		
		#assert
		self.assertIsNotNone(first_task)
		self.assertIs(first_task.user, self.user)
		self.assertIs(first_task.start_datetime, example_date)

		
	def test_delete_plan__created_tasks_added_to_default_group(self):
		#arrange
		example_date = datetime.datetime(2018, 12, 11)
		task =  pm.create_plan('plan', example_date, Plan.Period.MONTHLY, self.user)
		
		#act
		pm.delete_plan(task.planner)
		
		#assert
		self.assertIn(task, self.user.default_group.tasks.all())

