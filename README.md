# task_tracker
Task tracker is a simple tool that you can use in order to orginize your life. You can shedule tasks, create plans, groups and share them with other people.

##Installation
In order to install the program, firstly you should download the repository. Change the directory to
some folder, that will contain the project and enter the following:

`git clone https://bitbucket.org/DziyanaKhodar/task_tracker`

Than type:

`sudo pip3 install .`

After that, you will be able to use the tracker from any directory by the keyword 'track'.

#Usage

This chapter represents a brief guide for using task tracker from console. You can learn more about operations on each of entities type by typing --help, for example:

`track task --help`

Or you can just enter:

`track task`

And help information will be shown.

##Start working

First of all you need to create a new user and login.
You can do this simply by typing:

`track user add Jack`
`track user login Jack`

##Working with tasks:
Here is a simle sample of creating a new task. You can specify different parameters for the task, such as deadline, descriprion, priority etc.

`track task add 'My first task'`

or 

`track task add 'My second task' --finish-time 2018-06-28 --priority HIGH`

After adding a task it's short description and id will be shown on the screen. Use id in order to perform different operation on your tasks.
For example, id of 'My first task' was 1 and id of 'My second task' was 2.
So you would like to describe the task in more detailed manner, you can attach subtasks, then just type:

`track task add-subtasks 1 2`

You can put a task one level higher in the tree if needed:

`track task level-up 2`

##Working with groups
The same way it was done with tasks, you can create a group. Group is a container for tasks, that can also be shared with other users in order to orginize team work.

`track group add 'My awesome group'`

Now you can add tasks to the new group and share the group with another user.
For example, if new group id is 6, you can type:

`track task add-to-groups 2 6` in order to add task 2 to group 6

and 

`track group add-users 6 myfriend` in order to add your friend to group 6

So the user can now see and edit your task, and add his own ones.

You can assign tasks in the group to the user, so that he will become their executor.

`track user assign myfriend 2`

##Working with plans
If you have some tasks, that you need to  do every week or every day, you can create a plan.
To do that, you need to define the plan name, the date when the first task should be started and period(for example, dayly or monthly)
And there you will have a first planned task:

`track plan add 'Go to the gym' 2018-06-01 WEEKLY`











